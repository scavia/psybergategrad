package jee.ce1;

import jee.date.Date;

public class MyDateImplementation implements Date {

	int day;
	int month;
	int year;

	public MyDateImplementation(int year, int month, int day) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}

	@Override
	public int getYear() {
		return year;
	}

	@Override
	public int getMonth() {
		return month;
	}

	@Override
	public int getDay() {
		return day;
	}

	@Override
	public boolean isLeapYear() {
		if (getYear() % 100 == 0 || getYear() % 400 == 0 &&  getYear() % 4 == 0) {
			return true;
		} else
			return false;
	}

	@Override
	public Date addDays(int numDays) {
		return new MyDateImplementation(year, month, day);
	}

	@Override
	public String toString() {
		if (getYear() < 0) {
			return "Date [" + (-1*getYear()) + "BC" + "]";
		} else
			return "Date [" + getYear() + "-" + getMonth() + "-" + getDay() + "]";
	}

}

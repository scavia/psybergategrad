package jee.ce1;

import static org.junit.Assert.*;

import org.junit.Test;

import jee.date.Date;
import jee.date.DateFactory;
import jee.date.InvalidDateException;

public class MyDateTest {

	private DateFactory factory = new MyDateFactory();

	@Test(expected = InvalidDateException.class)
	public void exceptionTestYear() throws InvalidDateException {
		factory.createDate(3001, 1, 14);
	}

	@Test(expected = InvalidDateException.class)
	public void exceptionTestMonth() throws InvalidDateException {
		factory.createDate(1999, 13, 2);
	}

	@Test(expected = InvalidDateException.class)
	public void exceptionTestNegative() throws InvalidDateException {
		Date output = factory.createDate(1999, -1, 2);
		assertEquals(output, output);
	}

	@Test
	public void exceptionTestDay() {
		try {
			// ctrl+1 - extract local variable
			// alt sift i - move bac declarations after extractions
			// alt sif m - extract metod
			factory.createDate(1999, 4, 34);
		} catch (InvalidDateException expected) {
			// pass if it propagates exception if you did not put expected on
			// test annotation
		}
	}

	@Test
	public void exceptionTestDayNegative() {
		//many invalid dates
		createInvalidDate(1999, 4, -1);
		createInvalidDate(1999, 20, 1);
		createInvalidDate(-100, 0, 5);
		createInvalidDate(1999, -1, 0);
		createInvalidDate(1999, 4, 32);
		createInvalidDate(3001, 10, 6);
	}

	private void createInvalidDate(int year, int month, int day) {
		try {
			factory.createDate(year, month, day);
			fail("not valid");
		} catch (InvalidDateException expected) {
		}
	}

	@Test
	public void testLeapYear() throws InvalidDateException {
		factory.createDate(1999, 10, 2);
		assertFalse("Not a leap year", 1999 % 4 == 0);
		assertFalse("Not a leap year", 1999 % 100 == 0);
		assertFalse("Not a leap year", 1999 % 400 == 0);
	}

}

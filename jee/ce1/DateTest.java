package jee.ce1;

import jee.date.Date;
import jee.date.DateFactory;
import jee.date.InvalidDateException;

public class DateTest {

	public static void main(String[] args) throws InvalidDateException {
		
		//instantiate tru interfaces implemented
		DateFactory df = new MyDateFactory();
		Date d = df.createDate(2019, 4, 20);
		System.out.println(d.getClass());
		System.out.println(d);
		try {
			System.out.println(df.createDate((-2000), 12, 30));
		} catch (InvalidDateException e) {
			System.out.println(e.getDateErrorCode() + ": " + e.getMessage());
		}
	}

}

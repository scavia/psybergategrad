package jee.ce1;

import jee.date.Date;
import jee.date.DateFactory;
import jee.date.InvalidDateException;

public class MyDateFactory implements DateFactory {

	@Override
	public Date createDate(int year, int month, int day) throws InvalidDateException {
		if (!isValidYear(year)) {
			throw new InvalidDateException(3, "Invalid Year");
		}
		if (!isValidMonth(month)) {
			throw new InvalidDateException(2, "Invalid Month");
		}
		if (!isValidDay(day)) {
			throw new InvalidDateException(1, "Invalid Day");
		}
		if (!isValidFebruary(day, month)) {
			throw new InvalidDateException(1, "Invalid Day");
		}
		return new MyDateImplementation(year, month, day);
	}

	private boolean isValidYear(int year) {
		return (year <= 3000);
	}

	private boolean isValidMonth(int month) {
		return (month <= 12 && month >= 1);
	}

	private boolean isValidDay(int day) {
		return (day <= 31 && day >= 1);
	}
	
	private boolean isValidFebruary(int day, int month) {
		return (month == 2 && day <= 29);
	}

}

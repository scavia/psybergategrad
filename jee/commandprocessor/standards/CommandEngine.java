package jee.commandprocessor.standards;

public interface CommandEngine {

	public void processRequest();

}

package jee.commandprocessor.standards;

public interface CommandResponse {

	public void sendResponse();

}

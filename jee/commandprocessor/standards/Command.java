package jee.commandprocessor.standards;

public interface Command {
	
	public void execute();
	
}

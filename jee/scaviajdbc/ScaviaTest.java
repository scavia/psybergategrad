package jee.scaviajdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ScaviaTest {

	private static String url = "";
	private static String username = "";
	private static String password = "";
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("jee.scaviajdbc.ScaviaDriver");
		System.out.println("registering...");
		Connection conn = DriverManager.getConnection(url, username, password);
		//conn pointing to an instance of new scaviaconnection
		System.out.println(conn.getClass());
		Statement state = conn.createStatement();
		//statement pointing to an instance of scaviastatement;
		System.out.println(state.getClass());
		//rs pointing to an instance of scaviaresultset;
		ResultSet rs = state.executeQuery("select * from mytable");
		System.out.println(rs.getClass());
		System.out.println("done..");
	}

}

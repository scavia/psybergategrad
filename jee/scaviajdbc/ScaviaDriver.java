package jee.scaviajdbc;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

public class ScaviaDriver implements Driver {
	static {

		try {
			Class.forName("jee.scaviajdbc.ScaviaDriver");
			ScaviaDriver inst = new ScaviaDriver();
			DriverManager.registerDriver(inst);
		} catch (ClassNotFoundException | SQLException e) {
			// you can throw a RuntimeExeption(e) wrap the checked exception
			// since it can be put on static signature
			// therefore execption initializer error thrown and class is not
			// loaded because error happened in a static block
			throw new RuntimeException(e);
			}

	}

	@Override
	public Connection connect(String url, Properties info) throws SQLException {
		// RemoteDriver remoteDriver = url;
		// if (remoteDriver == null) {
		// remoteDriver = (RemoteDriver)Naming.lookup("");
		// }
		return new ScaviaConnection();
	}

	@Override
	public boolean acceptsURL(String url) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getMajorVersion() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMinorVersion() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean jdbcCompliant() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}
}

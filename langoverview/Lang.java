package langoverview;

public class Lang {
	
	//instance variable initializer (saing its equal to)
	//class variable in maxvalue apllies to wole class
	public final static int MAX_VALUE=33;
	String s = "abc";
	Object o = new Object();
	
	//constructor
	public Lang(){
		System.out.println("constructor called");
	}
	
	//static initializer
	static{
		System.out.println("Max for class->Lang STATIC INIT:" + MAX_VALUE);
		new RuntimeException("Zero can't divide!");
	}
	
	//method
	public static void DoSometing(){
		//local variable cn't be static oterwise it wont cmpile int s(delared witin curly braces)
		for(int s=1; s<2; s++){
		System.out.println("-------------------------------");
		System.out.println("dosometin() from Lang class");
		}
		//local variables a&b
		int a=7,b=70;
		//operaters
		if(!(a>=b)){
			System.out.println("max is:" +b);
		}
		else{
			System.out.println("max is:" +a);
		}
		
	}
	
	//instance initializer
	{
		System.out.println("instance initializer called");
	}
	
	public static void main(String[] args) throws Exception{
		//Lang.DoSometing();
		//Scaa.See();
		//Scavia u = new Scavia();
		//Scavia.Complete();
		//obect variable
		Scavia u = new Scavia();
		u.Complete();
		
		new Lang();
		//new Scavia();
		Lang.DoSometing();
		Scaa.See();
		//Scavia.Complete();

	}

}

//final class Scavia extends Lang{
final class Scavia {
	
	//static variable is initialised once and supposed to be capital letters
	public final static int MAX_VALUE=50;
	public static int MAX_VALU=4;
	
	static{
		System.out.println("");
		System.out.println("Max for class->Scavia is:" + MAX_VALUE);
	}
	
	public final static void Complete(){
		int c=1;
		do
		{
			System.out.println("*******************************");
			System.out.println("I am Complete() from Scavia CLASS");
			c++;
		}
		while(c<2);
		
		//terenary operator
	      int a, b;
	      a = 10;
	    //if condition b4 ques mark true it prints 20 if false 30 it acts a boolean operator
	      b = (a == 1) ? 20: 30;
	      System.out.println( "Value of b is : " +  b );
	    //variable x = (expression) ? value if true: value if false
	      b = (a == 10) ? 20: 30;
	      System.out.println( "Value of b is : " + b );
		
	}
	
	public static void main(String[] args){
		
		Scavia.Complete();
		Lang.DoSometing();
		System.out.println(new Scavia().MAX_VALUE);
		System.out.println(new Scavia().MAX_VALU);
	}
	
}
	
abstract class Scaa{
		public static int See() throws Exception{
			int a=0, b=2;
			//runtime exception created
			if (a==0){
				throw new Exception("Zero can't divide!");
			}
			else{
				return (b/a);
			}
			
		}
		
		public static void main(String[] args) throws Exception{
			
			System.out.println("I am dividing 2by0 in Scaa Class");
			
			System.out.println(See());
			
		}
	
}


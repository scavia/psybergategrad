

public class method1test
{
     public static void main(String[ ] args)
    {
          greeting(5);  //first method call
          int response;
          response = getNumber();   //second method call
          greeting (response);          //first method call again
    }
 

     //First Method for greeting
     public static void greeting(int x)
    {
         int i;
         for(i = 0; i < x; i++)
         {
               System.out.print("Hi ");
         }
         System.out.println( );
    }


    //Second method which returns a number
    public static int getNumber( )
    {
         int number;
        do
        {
            number = 6;//Console.readInt("\nPlease enter value(1-10): ");
         }
        while ((number < 1) || (number > 10));  //error trap
        return number;  //return the number to main
    }
}
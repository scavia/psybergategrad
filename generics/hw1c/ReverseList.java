//package generics.hw1c;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
//public class ReverseList {
//
//	public static <T> List<T> reverseList(List<T> list) {
//
//		List<T> reverse = new ArrayList<>(list);
//		Collections.reverse(reverse);
//		
//		for (int i = 0; i < reverse.size(); i++) {
//			((StringBuilder) reverse.get(i)).reverse();
//		}
//		return reverse;
//	}
//
//	public static void main(String[] args) {
//
//		// List<String> ls = Arrays.asList("bc","as","a");
//		// List<String> reverse = reverseList(ls);
//		// System.out.println(reverse);
//
//		List<StringBuilder> x = new ArrayList<StringBuilder>();
//		x.add(new StringBuilder("psybergate"));
//		x.add(new StringBuilder("scavia"));
//		x.add(new StringBuilder("software"));
//		x.add(new StringBuilder("developer"));
//
//		List<StringBuilder> u = reverseList(x);
//		System.out.println(u);
//	}
//}

package generics.hw1c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReverseList {

	@SuppressWarnings("unchecked")
	public static <T> List<T> reverseList(List<String> list) {

		List<String> reverse = new ArrayList<String>(list);
		Collections.reverse(reverse);

		List<String> sb = new ArrayList<>();
		for (int i = 0; i < reverse.size(); i++) {
			// sb = new ArrayList<>(); it then adds 1 last element
			sb.add(new StringBuilder((String) reverse.get(i)).reverse().toString());
		}
		return (List<T>) sb;
	}

	public static void main(String[] args) {

		List<String> x = new ArrayList<String>();
		x.add("psybergate");
		x.add("scavia");
		x.add("software");
		x.add("developer");

		List<String> u = reverseList(x);
		System.out.println(u);
	}
}

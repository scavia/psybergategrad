package generics.hw2;

import java.util.ArrayList;
import java.util.List;

public class IntegerList {

	public static <T> List<T> greaterTan1000(List<T> list) {

		int count = 0;
		List<T> newColl = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			if ((Integer) list.get(i) > 1000) {
				// System.out.println(list.get(i));
				newColl.add(list.get(i));
				count++;
			}
		}
		System.out.println("Numbers > 1000: "+ count);
		return newColl;
	}

}

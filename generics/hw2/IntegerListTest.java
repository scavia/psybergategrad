package generics.hw2;

import java.util.ArrayList;
import java.util.List;

public class IntegerListTest {
	public static void main(String[] args) {
		List<Integer> x = new ArrayList<Integer>();
		x.add(100);
		x.add(5000);
		x.add(3000);
		x.add(20);
		x.add(800);
		x.add(10000);

		List<Integer> u = IntegerList.greaterTan1000(x);
		System.out.println(u);

	}
}

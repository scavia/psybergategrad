package generics.hw3a;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Homework3a {

	public static <T> List<T> evenIntegerCollection(List<T> list) {

		// List<T> even = new ArrayList<>(list);
		int count = 0;
		List<T> newColl = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			if ((Integer) list.get(i) % 2 == 0) {
				// System.out.println(list.get(i));
				newColl.add(list.get(i));
				count++;
			}
		}
		System.out.println("Number of even numbers: " + count);
		return newColl;
	}

	public static <T> List<T> oddIntegerCollection(List<T> list) {

		// List<T> odd = new ArrayList<>(list);
		int count = 0;
		List<T> newColl = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			if ((Integer) list.get(i) % 2 != 0) {
				// System.out.println(odd.get(i));
				newColl.add(list.get(i));
				count++;
			}
		}
		System.out.println("Number of odd numbers: " + count);
		return newColl;
	}

	public static <T> int primeIntegerCollection(List<T> list) {

		// List<T> prime = new ArrayList<>(list);
		int count = 0;
		List<T> newColl = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			for (int m = 2; m <= Math.sqrt((Integer) list.get(i)); m += 2) {
				if ((Integer) list.get(i) % m == 0) {
					break;

				} else {
					// System.out.println(prime.get(i));
					newColl.add(list.get(i));
					count++;
				}
			}
		}
		System.out.println("Number of prime numbers: " + count);
//		return newColl;
		return count;
	}

	public static <T> List<T> stringCollection(Collection<T> list) {

		List<T> prime = new ArrayList<>(list);
		int count = 0;
		List<T> newColl = new ArrayList<>();
		for (int i = 0; i < prime.size(); i++) {
			if (((String) prime.get(i)).toLowerCase().charAt(0) == 'c') {
				// System.out.println(prime.get(i));
				newColl.add(prime.get(i));
				count++;
			}
		}
		System.out.println("Number of strings that start with small c: " + count);
		return newColl;
	}
}

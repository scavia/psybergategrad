package generics.hw3a;

import java.util.ArrayList;
import java.util.List;

public class Homework3aTest {

	public static void main(String[] args) {
		List<Integer> x = new ArrayList<Integer>();
		x.add(100);
		x.add(5000);
		x.add(3000);
		x.add(331);
		x.add(25);
		x.add(20);
		x.add(27);
		x.add(801);
		x.add(800);
		x.add(10000);
		
		List<Integer> e = Homework3a.evenIntegerCollection(x);
		System.out.println(" ");
		System.out.println("Even list:\n"+ e);
		
		List<Integer> o = Homework3a.oddIntegerCollection(x);
		System.out.println(" ");
		System.out.println("Odd list:\n"+ o);
		
		int p = Homework3a.primeIntegerCollection(x);
		System.out.println(" ");
		System.out.println("Prime list:\n"+p);
		
		List<String> s = new ArrayList<String>();
		s.add("carlos");
		s.add("darlos");
		s.add("calvis");
		
		List<String> i = Homework3a.stringCollection(s);
		System.out.println(" ");
		System.out.println("String list:\n"+i);
	}

}

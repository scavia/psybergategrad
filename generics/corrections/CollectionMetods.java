package generics.corrections;

import java.util.Collection;

public class CollectionMetods {

	public static <T> int countCondition(Collection<T> collection, ConditionPredicate<T> condition) {

		int count = 0;
		for (T t : collection) {
			if (condition.testCondition(t)) {
				count++;
			}
		}
		return count;
	}
}

package generics.corrections;

public class EvenList implements ConditionPredicate<Integer> {

	@Override
	public boolean testCondition(Integer element) {
		return (element % 2 == 0);
	}

}

package generics.corrections;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionTest {

	public static void main(String[] args) {

		Collection<Integer> collection = new ArrayList<>();
		collection.add(10);
		collection.add(501);
		collection.add(123);
		collection.add(230);
		collection.add(70);
		collection.add(189);
		
		System.out.println(CollectionMetods.countCondition(collection, new EvenList()));
	}

}

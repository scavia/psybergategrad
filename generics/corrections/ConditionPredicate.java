package generics.corrections;

public interface ConditionPredicate<T> {

	public abstract boolean testCondition(T element);
}

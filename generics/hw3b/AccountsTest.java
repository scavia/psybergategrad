package generics.hw3b;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class AccountsTest {

	public static void main(String[] args) {
		List<Accounts> accountList = new ArrayList<Accounts>();
		accountList.add(new Accounts("Abc", "AE001", -1490.0));
		accountList.add(new Accounts("Cde", "AE002", 100.0));
		accountList.add(new Accounts("Fgu", "AE003", 1500.0));
		accountList.add(new Accounts("Oni", "AE004", -1260.0));
		// System.out.println(accountList);

		@SuppressWarnings("rawtypes")
		List accLs = conditionTest(accountList, (Accounts u) -> u.getBalance() < 0);
		System.out.println(accLs);
	}

	private static <T> List<Accounts> conditionTest(List<T> list, Predicate<Accounts> pred) {
		List<Accounts> result = new ArrayList<Accounts>();
		for (T acc : list)
			if (pred.test((Accounts) acc))
				result.add((Accounts) acc);
		return result;
	}
}

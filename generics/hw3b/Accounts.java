package generics.hw3b;

public class Accounts {
	
	private String name;
	
	private String accNum;
	
	private double balance;
	
	public Accounts(String name, String accNum, double balance) {
		super();
		this.name = name;
		this.accNum = accNum;
		this.balance = balance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccNum() {
		return accNum;
	}

	public void setAccNum(String accNum) {
		this.accNum = accNum;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "\nName = " + getName() + ", AccNum = " + getAccNum() + ", Balance = " + getBalance()
				;
	}

	public boolean equals(Object anObject) {
		return accNum.equals(anObject);
	}
	
}

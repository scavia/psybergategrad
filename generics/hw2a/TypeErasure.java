package generics.hw2a;

import java.util.ArrayList;
import java.util.List;

public class TypeErasure {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		List x = new ArrayList();
		x.add(100.55550);
		x.add(5000);
		x.add(3000);
		x.add(20);
		x.add(800);
		x.add(10000);

		// int u = ((Double) x.get(0)).intValue();
		int m = ((Integer) x.get(1)).intValue();

		// System.out.println(u);
		System.out.println(m);
	}

}

//package generics.hw1a;
//
//public class MyMap {
//
//	private Object key;
//	private Object value;
//
//	public MyMap(Object key, Object value) {
//		super();
//		this.key = key;
//		this.value = value;
//	}
//
//	public Object getKey() {
//		return key;
//	}
//
//	public void setKey(Object key) {
//		this.key = key;
//	}
//
//	public Object getValue() {
//		return value;
//	}
//
//	public void setValue(Object value) {
//		this.value = value;
//	}
//
//}
package generics.hw1a;

public class MyMap<K, V> {
	
	private K key;
	private V value;
	
	public MyMap(K key, V value) {
		super();
		this.key = key;
		this.value = value;
	}
	
	public K getKey() {
		return key;
	}
	
	public void setKey(K key) {
		this.key = key;
	}
	
	public V getValue() {
		return value;
	}
	
	public void setValue(V value) {
		this.value = value;
	}
	
}

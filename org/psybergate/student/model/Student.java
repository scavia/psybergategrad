package org.psybergate.student.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.psybergate.student.interfaces.StudentInterface;

@Entity
@Table
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "StudentType")
public abstract class Student implements StudentInterface, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String studentnumber;

	@Column(name = "studentname")
	private String studentname;

	@Column(name = "studentsurname")
	private String studentsurname;

	@Column(name = "faculty")
	private String faculty;

	@Column(name = "fees")
	private int fees;

	public Student() {

	}

	public Student(String studentnumber, String studentname, String studentsurname, String faculty, int fees) {
		super();
		this.studentnumber = studentnumber;
		this.studentname = studentname;
		this.studentsurname = studentsurname;
		this.faculty = faculty;
		this.fees = fees;
	}

	public String getStudentnumber() {
		return studentnumber;
	}

	public void setStudentnumber(String studentnumber) {
		this.studentnumber = studentnumber;
	}

	public String getStudentname() {
		return studentname;
	}

	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}

	public String getStudentsurname() {
		return studentsurname;
	}

	public void setStudentsurname(String studentsurname) {
		this.studentsurname = studentsurname;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public int getFees() {
		return fees;
	}

	public void setFees(int fees) {
		this.fees = fees;
	}

	public void depositFees(int amount) {
		fees += amount;
	}

	@Override
	public String toString() {
		return "\nStudent [Studentnumber: " + getStudentnumber() + ", Studentname: " + getStudentname()
				+ ", Studentsurname: " + getStudentsurname() + ", Faculty: " + getFaculty() + ", Fees: R" + getFees()
				+ ", StudentCode: " + hashCode() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((studentnumber == null) ? 0 : studentnumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (studentnumber == null) {
			if (other.studentnumber != null)
				return false;
		} else if (!studentnumber.equals(other.studentnumber))
			return false;
		return true;
	}

}
package org.psybergate.student.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "Undergraduate")
@DiscriminatorValue(value = "undergraduate")
public class Undergraduate extends Student {

	private static final long serialVersionUID = 1L;
	public static final int REGISTRATION_FEE = 50;

	public Undergraduate() {
		super();
	}

	public Undergraduate(String studentnumber, String studentname, String studentsurname, String faculty, int fees) {
		super(studentnumber, studentname, studentsurname, faculty, fees);
	}

	@Override
	public void depositFees(int amount) {
		amount += REGISTRATION_FEE;
		super.depositFees(amount);
	}

}

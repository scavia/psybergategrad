package org.psybergate.student.service;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.psybergate.student.condition.TestCollectionMetod;
import org.psybergate.student.condition.TestMetod;
import org.psybergate.student.customsorts.SortByName;
import org.psybergate.student.customsorts.SortByStudentNumber;
import org.psybergate.student.exceptions.StudentException;
import org.psybergate.student.model.Postgraduate;
import org.psybergate.student.model.Student;
import org.psybergate.student.model.Undergraduate;

public class StudentServices {

	private Scanner inputDetails;

	private static final int MINIMUM_REGISTRATION_FEE = 5000;

	private static final String PERSISTENCE_UNIT_NAME = "Student";

	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

	private EntityManager em = factory.createEntityManager();

	public StudentServices() {
		inputDetails = new Scanner(System.in);
	}

	public void createStudentRecord() throws StudentException {
		String studentnumber;
		String name;
		String surname;
		String faculty;
		int fees;

		System.out.println("|-----REGISTER NEW STUDENT-----| \nStudent Number: ");
		studentnumber = inputDetails.next();
		System.out.println("Student Name: ");
		name = inputDetails.next();
		System.out.println("Student Surname: ");
		surname = inputDetails.next();
		System.out.println("Faculty: ");
		faculty = inputDetails.next();
		System.out.println("Fees: ");
		fees = inputDetails.nextInt();

		String input = studentnumber;
		Pattern pattern = Pattern.compile("[a-j]{1}[0-9]{3}");
		Matcher matcher = pattern.matcher(input);
		Pattern pattern1 = Pattern.compile("[k-z]{1}[0-9]{3}");
		Matcher matcher1 = pattern1.matcher(input);
		matcher.find();
		matcher1.find();

		if (matcher.group() == input && fees > MINIMUM_REGISTRATION_FEE
				&& studentnumber.toLowerCase().charAt(0) <= 'j') {
			createUndergraduateStudent(studentnumber, name, surname, faculty, fees);
		} else if (matcher1.group() == input && fees > MINIMUM_REGISTRATION_FEE
				&& studentnumber.toLowerCase().charAt(0) > 'j') {
			createPostgraduateStudent(studentnumber, name, surname, faculty, fees);
		} else {
			throw new StudentException();
		}
	}

	public void createUndergraduateStudent(String studentnumber, String studentname, String studentsurname,
			String faculty, int fees) {
		em.getTransaction().begin();
		em.persist(new Undergraduate(studentnumber, studentname, studentsurname, faculty, fees));
		em.getTransaction().commit();
	}

	public void createPostgraduateStudent(String studentnumber, String studentname, String studentsurname,
			String faculty, int fees) {
		em.getTransaction().begin();
		em.persist(new Postgraduate(studentnumber, studentname, studentsurname, faculty, fees));
		em.getTransaction().commit();
	}

	public void printStudents() {
		Query getStudents = em.createQuery("select students from Student students");
		@SuppressWarnings("unchecked")
		List<Student> studentList = getStudents.getResultList();
		System.out.println("Number of registered students: " + studentList.size());
		System.out.println("\nUNSORTED STUDENTS FROM DATABASE:");
		System.out.println("__________________________");
		for (Student student : studentList) {
			System.out.println(student);
		}

		System.out.println("\nSORTED STUDENTS BY STUDENT NUMBER:");
		System.out.println("__________________________________");
		Collections.sort(studentList, new SortByStudentNumber());

		for (Student student : studentList) {
			System.out.println(student);
		}

		System.out.println("\nSORTED STUDENTS BY NAME:");
		System.out.println("________________________");
		Collections.sort(studentList, new SortByName());

		for (Student student : studentList) {
			System.out.println(student);
		}

	}

	public void feeDeposit() throws StudentException {
		String studentnumber;
		int amount;

		System.out.println("\n|----Pay fees----|\nStudent number: ");
		studentnumber = inputDetails.next();
		System.out.println("Fees: ");
		amount = inputDetails.nextInt();

		if (amount > 0) {
			depositStudentFees(studentnumber, amount);
		} else {
			throw new StudentException("Can deposit amount less than or equal to zero...");
		}
	}

	public void depositStudentFees(String studentnumber, int amount) throws StudentException {
		Query getStudent = em.createQuery("select students from Student students");

		@SuppressWarnings("unchecked")
		List<Student> studentList = getStudent.getResultList();
		boolean checkStudentNumber = false;
		for (Student student : studentList) {
			if (student.getStudentnumber().equals(studentnumber)) {
				checkStudentNumber = true;
				student.depositFees(amount);
				em.getTransaction().begin();
				em.createQuery("Update Student set fees = " + student.getFees() + " where studentnumber = '"
						+ studentnumber + "'").executeUpdate();
				em.getTransaction().commit();
				System.out.println("\nFees of 'R" + amount + "' deposited to Student Number : "
						+ student.getStudentnumber() + "\nFees Statement: R" + student.getFees());
			}
		}
		if (!checkStudentNumber) {
			throw new StudentException("Student does not exist...");
		}
	}

	public void feesBelowHalf() {
		Query getStudents = em.createQuery("select students from Student students");
		@SuppressWarnings("unchecked")
		List<Student> studentList = getStudents.getResultList();
		TestCollectionMetod test = new TestCollectionMetod();
		System.out.println("View Student with fees less than half:");
		System.out.println("_____________________________________");
		System.out.println("\n" + test.selectConditionBased(studentList, new TestMetod()));
	}

}

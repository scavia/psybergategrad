package org.psybergate.student.view;

import java.util.Scanner;

import org.psybergate.student.controller.StudentController;
import org.psybergate.student.exceptions.StudentException;

public class StudentApplicationMain {

	private Scanner choice;
	private boolean exit;
	private StudentController controller = new StudentController();

	public static void main(String[] args) {
		StudentApplicationMain run = new StudentApplicationMain();
		run.displayMenu();
	}

	private void displayMenu() {
		displayHeader();
		while (!exit) {
			printMenu();
			int selection = getInput();
			perfomSelection(selection);

		}
	}

	private void displayHeader() {
		System.out.println("+------------------------------------------+");
		System.out.println("|           STUDENT REGISTRATION.          |");
		System.out.println("+------------------------------------------+");
	}

	private void printMenu() {
		System.out.println("\n	...Make Selection Below...	");
		System.out.println("1.	Register Student.");
		System.out.println("2.	Deposit Fees.");
		System.out.println("3.	View Students.");
		System.out.println("4.	View Students with fees less than half.");
		System.out.println("5.	Exit.");
	}

	private int getInput() {
		choice = new Scanner(System.in);
		int selection = 0;

		while (selection < 1 || selection > 5) {
			try {
				System.out.println("\nSelect choice.");
				selection = Integer.parseInt(choice.nextLine());
			} catch (Exception e) {
				throw new IllegalArgumentException();
			}
		}

		return selection;
	}

	private void perfomSelection(int selection) {
		switch (selection) {
		case 1:
			try {
				controller.sendCreateCommand();
				System.out.println("\n____________________");
				System.out.println("\nStudent Registered...");
			} catch (StudentException | IllegalStateException e) {
				System.out.println("\n_____________________________");
				System.out.println("\nFailed to Register Student...!");
				System.out.println("Student number must begin with one letter followed by 3 digits...");
				System.out.println("Minimum student registration fee is R5000...");
			}
			break;

		case 2:
			try {
				controller.sendDepositCommand();
				System.out.println("\n_____________________");
				System.out.println("\nFees Deposit Successful...");
			} catch (StudentException e) {
				System.out.println("\n_____________________________");
				System.out.println("\nFailed to Deposit...!");
				System.out.println(e.getMessage());
			}
			break;

		case 3:
			controller.sendViewAllAccounts();
			break;

		case 4:
			controller.sendViewStudCommand();
			break;

		case 5:
			exit = true;
			System.out.println("\nThank you, Bye Bye.");
			break;

		default:
			System.out.println("\nFatal Error.");
			break;
		}
	}
}

package org.psybergate.student.controller;

import org.psybergate.student.exceptions.StudentException;
import org.psybergate.student.service.StudentServices;

public class StudentController {
	private StudentServices service;

	public StudentController() {
		service = new StudentServices();
	}

	public void sendCreateCommand() throws StudentException {
		service.createStudentRecord();
	}

	public void sendDepositCommand() throws StudentException {
		service.feeDeposit();
	}

	public void sendViewAllAccounts() {
		service.printStudents();
	}

	public void sendViewStudCommand() {
		service.feesBelowHalf();
	}

}

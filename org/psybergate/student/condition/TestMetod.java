package org.psybergate.student.condition;

import org.psybergate.student.model.Student;

public class TestMetod implements ViewCondition<Student> {

	private static final int HALF_FEES = 8500;
	@Override
	public boolean conditionToMeet(Student element) {

		return (element.getFees() < HALF_FEES);
	}

}

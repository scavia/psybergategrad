package org.psybergate.student.condition;

public interface ViewCondition<T> {

	public abstract boolean conditionToMeet(T element);
}

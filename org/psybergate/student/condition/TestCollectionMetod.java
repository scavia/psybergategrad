package org.psybergate.student.condition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TestCollectionMetod {
	public <T> List<T> selectConditionBased(Collection<T> collection, ViewCondition<T> condition) {
		List<T> list = new ArrayList<>();
		for (T t : collection) {
			if (condition.conditionToMeet(t)) {
				list.add(t);
			}
		}
		return list;
	}
}

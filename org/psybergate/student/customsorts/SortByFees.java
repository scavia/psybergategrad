package org.psybergate.student.customsorts;

import java.util.Comparator;

import org.psybergate.student.model.Student;

public class SortByFees implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		// sort by ascending
		return (o1.getFees() - o2.getFees());
	}

}

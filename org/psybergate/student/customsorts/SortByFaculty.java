package org.psybergate.student.customsorts;

import java.util.Comparator;

import org.psybergate.student.model.Student;

public class SortByFaculty implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		//ascending order
		return (o1.getFaculty().compareTo(o2.getFaculty()));
	}

}

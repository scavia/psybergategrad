package org.psybergate.student.customsorts;

import java.util.Comparator;

import org.psybergate.student.model.Student;

public class SortByStudentNumber implements Comparator<Student>{

	@Override
	public int compare(Student o1, Student o2) {
		//ascending order
		return (o1.getStudentnumber().compareTo(o2.getStudentnumber()));
	}

}

package oodatevalidate;

public class DateValidate {

	int day;
	int month;
	int year;

	DateValidate(int d, int m, int y) {
		day = d;
		month = m;
		year = y;
	}

	public void display() {
		if (day > 0 && day < 32 && month > 0 && month < 13 && year > 1000) {
			System.out.println(day + "-" + month + "-" + year);
		} else {
			System.out.println("*********INVALIDITY ALERT***********");
			System.out.println(/* day+"-"+month+"-"+year+" :"+ */"Invalid Date...");
		}
	}

	public Boolean equals(DateValidate DV) {
		if (DV == null)
			return false;
		return this.day == DV.day;
	}

	public static void main(String[] args) {

		DateValidate DV1 = new DateValidate(20, 12, 1001);
		DV1.display();

		DateValidate DV2 = DV1;
		DV2.display();

		DateValidate DV3 = new DateValidate(20, 11, 2001);
		DV3.display();

		System.out.println("++++++++++++Boolean+++++++++++");
		System.out.println(DV2 == DV1);
		System.out.println(DV3 == DV1);
		System.out.println(DV3.equals(DV1));
	}

}

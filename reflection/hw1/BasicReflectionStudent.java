package reflection.hw1;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import oopolymorph.hw4.Student;

public class BasicReflectionStudent {

	public static void main(String[] args) throws ClassNotFoundException {
		Class<?> studsuperclass = Student.class;
		studsuperclass = Class.forName("reflection.ce1.Student").getSuperclass();

		System.out.println("SuperClass of Student:\t" + studsuperclass);

		System.out.println("Class modifiers:\t" + Modifier.toString(studsuperclass.getModifiers()));

		System.out.println("******************fields************************");
		Field[] fields = Class.forName("reflection.ce1.Student").getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			Field f = fields[i];
			String name = f.getName();
			
			System.out.println(Modifier.toString(f.getModifiers()) + " " + name);
		}

		System.out.println("******************metods************************");
		Method[] met = Class.forName("reflection.ce1.Student").getDeclaredMethods();
		for (int i = 0; i < met.length; i++) {
			Method m = met[i];
			String name = m.getName();
			System.out.println(Modifier.toString(m.getModifiers()) + " " + name +" "+ m.getReturnType());
		}
	}

}

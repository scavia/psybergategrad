package reflection.ce1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class StudentReflectionTest {

	public static void main(String[] args) throws ClassNotFoundException {
		Class<?> studentclazz = Student.class;

		// create object and get class of object via reflection.
		studentclazz = new Student().getClass();

		/*
		 * for wrapper classes provide a static variable TYPE Class<?> cDouble =
		 * Double.TYPE; System.out.println(cDouble.getCanonicalName()); //
		 * prints double
		 */

		/*
		 * To get superclass you need .getSuperClass() studentclazz =
		 * Class.forName("reflection.ce1.Student").getSuperclass()
		 * System.out.println(Object.class.getSuperclass()); // prints "null"
		 */
		studentclazz = Class.forName("reflection.ce1.Student");

		System.out.println("Class of object:\t" + studentclazz.getCanonicalName());
		System.out.println("Package name:\t" + Class.forName("reflection.ce1.Student").getPackage().getName());
		System.out.println(
				"Annotations:\t" + Arrays.toString(Class.forName("reflection.ce1.Student").getDeclaredAnnotations()));
		System.out.println("Interfaces:\t" + Arrays.toString(Class.forName("reflection.ce1.Student").getInterfaces()));

		Field[] publicfields = Class.forName("reflection.ce1.Student").getFields();
		System.out.println("Public fields:\t" + Arrays.toString(publicfields));
		
		Field[] publicfields1 = Class.forName("reflection.ce1.Student").getDeclaredFields();
		System.out.println("Declared fields:\t" + Arrays.toString(publicfields1));

		Method[] method = Class.forName("reflection.ce1.Student").getMethods();
		System.out.println("Methods:\t" + Arrays.toString(method));
		
		Constructor<?>[] publicConstructors = Class.forName("reflection.ce1.Student").getConstructors();
		System.out.println("Constuctors:\t"+Arrays.toString(publicConstructors));
	}

}

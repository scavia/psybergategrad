package reflection.ce1;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Student")
public class Student implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String studentNum;

	@Column(name = "StudentName")
	private String studentName;

	@Column(name = "StudentSurname")
	private String studentSurname;

	@Column(name = "Age")
	private int age;

	@Column(name = "faculty")
	public String faculty;

	public Student() {
	
	}

	public Student(String studentNum, String studentName, String studentSurname, int age, String faculty) {
		super();
		this.studentNum = studentNum;
		this.studentName = studentName;
		this.studentSurname = studentSurname;
		this.age = age;
		this.faculty = faculty;
	}

	public Student(String studentNum, String studentName, String studentSurname, String faculty) {
		super();
		this.studentNum = studentNum;
		this.studentName = studentName;
		this.studentSurname = studentSurname;
		this.faculty = faculty;
	}

	public String getStudentNum() {
		return studentNum;
	}

	public void setStudentNum(String studentNum) {
		this.studentNum = studentNum;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentSurname() {
		return studentSurname;
	}

	public void setStudentSurname(String studentSurname) {
		this.studentSurname = studentSurname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public boolean borrowLibraryBook(String studentNum, String faculty, int status) {
		if (studentNum == getStudentNum() && faculty == getFaculty() && status <= 0) {
			System.out.println("No book borrowed...");
			// return false;
		} else if (studentNum == getStudentNum() && faculty == getFaculty() && status >= 1) {
			System.out.println("Borrowed book...");
			// return true;
		} else {
			System.out.println("No such student exists...");
		}
		return false;
	}

	@Override
	public String toString() {
		return "Student [Student Num:" + getStudentNum() + ", StudentName:" + getStudentName() + ", StudentSurname:"
				+ getStudentSurname() + ", Age:" + getAge() + ", Faculty:" + getFaculty() + "]";
	}

}

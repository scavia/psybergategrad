package reflection.ce2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DynamicLogicExe {

	public static void main(String[] args) throws NoSuchMethodException, SecurityException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		Constructor<?> constructor = Class.forName("reflection.ce1.Student").getConstructor(String.class, String.class,
				String.class, int.class, String.class);

		Object object = constructor.newInstance("R1924L", "Scavia", "Mlanga", 67, "Science");
		Method met = object.getClass().getMethod("borrowLibraryBook", String.class, String.class, int.class);
		met.invoke(object, "R1924L", "Science", 0);

		Constructor<?> constructor1 = Class.forName("reflection.ce1.Student").getConstructor();
		Object ob = constructor1.newInstance();
		Method met2 = ob.getClass().getDeclaredMethod("toString");
		System.out.println(met2.invoke(ob));

	}

}

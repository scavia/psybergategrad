package reflection.studentapplication;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import reflection.ce1.Student;

public class StudentApplication {

	private static final String PERSISTENCE_UNIT_NAME = "Student";
	private static EntityManagerFactory factory;

	public static void main(String[] args) {

		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = factory.createEntityManager();

		 em.getTransaction().begin();
		 em.persist(new Student("R1228L","Scavia","Mlanga",67,"Science"));
		 em.getTransaction().commit();

		Query query = em.createQuery("select student from Student student");
		@SuppressWarnings("unchecked")
		List<Student> studentList = query.getResultList();
		for (Student student : studentList) {
			System.out.println(student);
		}
		System.out.println("Size: " + studentList.size());

		em.close();
	}

	/// use comparator
	/// Iterator
	/// Generics
	/// predicate or condition
	/// OO
	/// create exception
	/// collections

}

package oocusomer;

public class customer {

	private int customer_num;

	private String cust_name;

	customer(int r, String n) {
		customer_num = r;
		cust_name = n;
	}

	// ***identity operater

	// overriding the parent equals method in the object class

	/*
	 * public boolean equals(customer r) { if (r == null) return false; return
	 * this.customer_num == r.customer_num; }
	 */

	// ***casting and overriding the parent equals method in the object class

	// public boolean equals(Object r){
	// if (r==null) return false;
	// return this.customer_num == ((customer)r).customer_num;
	// }

	public boolean equals(Object r) {
		if (this == r) {
			return true; // reflexivity check obect can not equal itself
		}

		if (r == null) {
			return false; // nullabity check whether if not null
		}
		
		System.out.println(System.identityHashCode(r)); //unique #/id
		System.out.println(r.hashCode()); //unique #/id

		//----------added code start
		if (this.getClass() != r.getClass())
			return false; // if not customer return false ineriting classes will
							// not pass test
		
		customer u = (customer)r; // cast
		
		return this.customer_num == u.customer_num;
		//--------- added code end

/*		if (r instanceof customer) { // ineriting classes will pass test
			customer c = (customer) r;
			return this.customer_num == c.customer_num;
		}

		return false;*/
	}

	public int getCustomer_num() {
		return customer_num;
	}

	public String getCust_name() {
		return cust_name;
	}

	public static void main(String[] args) {

		customer c1 = new customer(20, "scaa");
		customer c2 = c1;
		customer c3 = new customer(20, "scaa");
		System.out.println(c2 == c1);
		System.out.println(c3 == c1);
		System.out.println(c3.equals(c1));
		System.out.println(c3.equals(null));

	}

}

package collections.hw4;

public class ListTest {

	public static void main(String[] args) {
		CustomLinkedList cll = new CustomLinkedList();
		cll.addElement(2);
		cll.addElement(10);
		cll.addElement(5);
		cll.addElement(231);
		cll.addElement("abc");

		cll.getElement();
		System.out.println("\nSize of List: " + cll.getSize());
	}

}

package collections.hashmap;

public class Controller {

	private static AccountDB accountDB;
	private static AccountService service;

	public Controller() {
		accountDB = new AccountDB();
		// to matc accDB from service
		service = new AccountService(accountDB);
	}

	public void sendAccCreateCommand() throws AccountValidationException {
		accountDB.createAccount();
	}

	public void sendAccWitdrawCommand() throws AccountValidationException {
		service.witdraw();
	}

	public void sendAccViewCommand() throws AccountValidationException {
		System.out.println(accountDB.printAccounts());
	}
}

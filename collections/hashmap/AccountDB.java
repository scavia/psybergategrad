package collections.hashmap;

import java.util.HashMap;
import java.util.Scanner;

public class AccountDB {

	private HashMap<Integer, Account> accounts;

	private Scanner inpDetails;

	public AccountDB() {

		accounts = new HashMap<Integer, Account>();
		inpDetails = new Scanner(System.in);
	}

	public void createAccount() throws AccountValidationException {

		int clientNum;
		int accNum;
		String name;
		int balance;

		System.out.println("Enter details: \nClient number: ");
		clientNum = inpDetails.nextInt();
		System.out.println("Enter details: \nAccount number: ");
		accNum = inpDetails.nextInt();
		System.out.println("Customer name: ");
		name = inpDetails.next();
		System.out.println("Opening balance: ");
		balance = inpDetails.nextInt();

		if (accNum % 11 == 0 && accNum > 100 && balance > 1000) {
			insertAccount(clientNum, accNum, name, balance);
		} else {
			throw new AccountValidationException("Account number must be > 100, divisible by 11 & balance > 1000");
		}

	}

	public void insertAccount(int clientNum, int accountNum, String name, int openingBalance) throws AccountValidationException {
		Account account = new Account(accountNum, name, openingBalance);
//		if (accounts.contains(account)) {
//			throw new AccountValidationException("Account already exists...");
//		}
		accounts.put(clientNum, account);
	}

//	public void witdrawMoney(int clientNum, int amount) throws AccountValidationException {
//
//		boolean checkAcc = false;
//		for (Integer account : accounts.keySet()) {
//			if (account == clientNum) {
//				checkAcc = true;
//				account.withdraw(amount);
//				System.out.println("\nWitdraw on Account Number: " + accounts.get(clientNum));
//			}
//		}
//		if (!checkAcc) {
//			throw new AccountValidationException("Account doesn't exist...");
//		}
//	}

	public String printAccounts() {
		// cec size of arraylist accounts.size()
		System.out.println("\nNumber of accounts on system...: " + accounts.size());
		String details = "";
		for (Integer account : accounts.keySet()) {
			details += "______________________________";
			details += "\nClient Number: \t" + account + "\nDetails: " + accounts.get(account);
			details += "\n_____________________________";
		}
		return details.toUpperCase();

	}

}

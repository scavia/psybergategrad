package collections.hashmap;

import java.util.Scanner;

public class Runner {


	private static Controller controller = new Controller();

	public static void main(String[] args) throws AccountValidationException {
		Runner run = new Runner();
		run.displayMenu();
	}

	private boolean exit;
	private Scanner sc;

	public void displayMenu() throws AccountValidationException {
		printHead();
		while (!exit) {
			printMenu();
			int selection = getInput();
			perfomSelection(selection);

		}
	}

	private void printHead() {
		System.out.println("+-----------------------------+");
		System.out.println("|           BANKING.          |");
		System.out.println("+-----------------------------+");
	}

	private void printMenu() {
		System.out.println("\n	...Make Selection Below...	");
		System.out.println("1.	Create Account.");
		System.out.println("2.	Withdraw Money.");
		System.out.println("3.	View Account Details & Balance.");
		System.out.println("4.	Exit.");
	}

	private int getInput() {
		sc = new Scanner(System.in);
		int selection = 0;

		while (selection < 1 || selection > 4) {
			try {
				System.out.println("\nSelect choice.");
				selection = Integer.parseInt(sc.nextLine());
			} catch (Exception e) {
				throw new IllegalArgumentException();
			}
		}

		return selection;
	}

	private void perfomSelection(int selection) throws AccountValidationException {
		switch (selection) {
		case 1:

			try {
				controller.sendAccCreateCommand();
				System.out.println("\n____________________");
				System.out.println("\nAccount Created...");
			} catch (Exception e) {
				System.out.println("\n_____________________________");
				System.out.println("\nFailed to Create Account...!");
				System.out.println(e.getMessage());
			}

			break;
		case 2:
			try {
				controller.sendAccWitdrawCommand();
				System.out.println("\n_____________________");
				System.out.println("\nWitdraw Successful...");
			} catch (Exception ex) {
				System.out.println("\n_______________________");
				System.out.println("\nWitdraw Unsuccessful...");
				System.out.println(ex.getMessage());
			}
			break;
		case 3:

			try {
				controller.sendAccViewCommand();
			} catch (Exception e) {
				System.out.println("\n________________________");
				System.out.println("\nAccount doesnt exist...");
				System.out.println(e.getMessage());
			}

			break;
		case 4:
			exit = true;
			System.out.println("\nThank you, Bye Bye.");
			break;
		default:
			System.out.println("\nFatal Error.");

		}
	}

}


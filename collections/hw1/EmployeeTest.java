package collections.hw1;

import java.util.Collections;
import java.util.LinkedList;

public class EmployeeTest {

	public static void main(String[] args) {

		LinkedList<Employee> employee = new LinkedList<Employee>();

		employee.add(new Employee(1, "Abc"));
		employee.add(new Employee(37, "Defg"));
		employee.add(new Employee(20, "Gli"));
		employee.add(new Employee(15, "Sxz"));
		employee.add(new Employee(104, "Wqt"));
		employee.add(new Employee(2000, "Mnu"));

		System.out.println(employee);

		Collections.sort(employee);
		System.out.println("\nSorted Ascending: \n" + employee);
		Collections.sort(employee, Collections.reverseOrder());
		System.out.println("\nSorted Descending: \n" + employee);

	}

}

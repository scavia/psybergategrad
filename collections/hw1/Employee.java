package collections.hw1;

public class Employee implements Comparable<Employee> {

	private int employeenumber;

	private String employeename;

	Employee(int r, String n) {
		employeenumber = r;
		employeename = n;
	}

	public int getEmp_num() {
		return employeenumber;
	}

	public String getEmp_name() {
		return employeename;
	}

	public String toString() {
		return "|Employee Num: " + getEmp_num() + " Employee Name: " + getEmp_name() + "|";
	}

	public int compareTo(Employee o) {
		int comparedCustomerNum = o.employeenumber;
		if (this.employeenumber > comparedCustomerNum) {
			return 1;
		} else if (this.employeenumber == comparedCustomerNum) {
			return 0;
		} else {
			return -1;
		}
	}

}

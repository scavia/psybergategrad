package collections.myOwnStackImp;

//import java.util.Iterator;

public class ArrayStack implements Stack {

	private int size;

	private int position = 0;

	private int storage[];

	public ArrayStack(int size) {
		this.size = size;
		storage = new int[size];
	}

	public boolean add(Object e) {
		storage[position] = (int) e;
		position++;
		return false;
	}

	public boolean contains(Object e) {
		return false;
	}

	public boolean isEmpty() {
		if (position == 0) {
			return true;
		}
		return false;
	}

	public boolean isFull() {
		if (position == size) {
			return true;
		}
		return false;
	}

	public int size() {
		return position;
	}

//	public Iterator iterator() {
//		for (int i = 0; i < storage.length; i++) {
//
//		}
//		return null;
//	}

	public String toString() {
		if (isEmpty()) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.size; i++) {
			sb.append(this.storage[i] + " | ");
		}
		return sb.toString();
	}
}

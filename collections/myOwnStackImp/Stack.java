package collections.myOwnStackImp;

public interface Stack {
	public boolean add(Object e);

	public boolean contains(Object e);

	public boolean isEmpty();

	public int size();
}

package collections.myOwnStackImp;

public class ArrayStackTest {

	public static void main(String[] args) {
		ArrayStack s = new ArrayStack(3);
		s.add(3);
		s.add(7);
		s.add(7);
		System.out.println(s.toString());
		System.out.println("Size of stack: " + s.size());
		System.out.println("Is it empty: " + s.isEmpty());
		System.out.println("Is it full: " + s.isFull());
		System.out.println("Does it contain: " + s.contains(3));
	}

}

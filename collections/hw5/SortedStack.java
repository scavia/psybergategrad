package collections.hw5;

import java.util.Iterator;
import java.util.List;

public abstract class SortedStack implements List {

	public abstract boolean add(Object e);

	public abstract boolean contains(Object e);

	public abstract boolean isEmpty();

	public abstract boolean isFull();

	public abstract int size();

	public abstract Iterator iterator();

	public abstract String toString();

}

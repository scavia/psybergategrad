package collections.hw5;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class TreeStack extends SortedStack implements Comparable<TreeStack> {

	private int size;

	private int position = 0;

	private Object storage[];

	public TreeStack(int size) {
		this.size = size;
		storage = new Object[size];
	}

	public boolean add(Object e) {
		storage[position] = e;
		position++;
//		storage = toArray();
		List list = Arrays.asList(storage);
		Collections.sort(list);
		return false;
	}

	public boolean contains(Object e) {
		return false;
	}

	public boolean isEmpty() {
		if (position == 0) {
			return true;
		}
		return false;
	}

	public boolean isFull() {
		if (position == size) {
			return true;
		}
		return false;
	}

	public int size() {
		return position;
	}

	public Iterator iterator() {
		return null;
	}

	public String toString() {
		if (isEmpty()) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.size; i++) {
			sb.append(this.storage[i] + " | ");
		}
		return sb.toString();

	}
	
	public void sort(){
	
	}

	public int compareTo(TreeStack o) {
		return position;
//		int comparedPosition = o.position;
//		if (this.position > comparedPosition) {
//			return 1;
//		} else if (this.position == comparedPosition) {
//			return 0;
//		} else {
//			return -1;
//			}
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int index, Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object get(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object set(int index, Object element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(int index, Object element) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object remove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int indexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int lastIndexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}
}

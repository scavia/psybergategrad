package collections.hw5;

public class TreeStackTest {
	public static void main(String[] args) {
		TreeStack s = new TreeStack(5);
		s.add(3);
		s.add(7);
		s.add(56);
//		s.add(7);
		s.add("qeqe");
		System.out.println(s.toString());
		System.out.println("Size of stack: " + s.size());
		System.out.println("Is it empty: " + s.isEmpty());
		System.out.println("Is it full: " + s.isFull());
		System.out.println("Does it contain: " + s.contains(3));
		
//		System.out.println(s.compareTo(s));
		}

}

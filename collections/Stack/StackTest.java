package collections.Stack;

import java.util.*;

public class StackTest {

	// Java code for stack implementation

	// Pushing element on the top of the stack
	public static void stackpush(Stack<Integer> stack) {

		stack.push(24);
		stack.push(27);
		stack.push(27);
		stack.push(29);
		stack.push(279);
		stack.push(2709);
		stack.push(21219);

	}

	// Popping element from the top of the stack
	public static void stackpop(Stack<Integer> stack) {
		System.out.println("Pop :");

		for (int i = 0; i < 7; i++) {
			Integer y = (Integer) stack.pop();
			System.out.println(y);
		}
//		System.out.println(stack.pop());
//		System.out.println(stack.pop());
	}

	// Displaying element on the top of the stack
	public static void stackpeek(Stack<Integer> stack) {
		System.out.println("Element on stack top : " +stack.peek());
	}

	// Searching element in the stack
	public static void stacksearch(Stack<Integer> stack, int element) {
		Integer pos = (Integer) stack.search(element);
		//System.out.println(stack.search(element));
		if (pos == -1)
			System.out.println("Element not found");
		else
			System.out.println("Element is found at position " + pos);
	}

	public static void main(String[] args) {
		Stack<Integer> stack = new Stack<Integer>();

		stackpush(stack);
		System.out.println("*after push"+ stack);
		stackpop(stack);
		System.out.println("*after pop"+ stack);
		stackpush(stack);
		System.out.println("*after 2nd push"+ stack);
		stackpeek(stack);
		stacksearch(stack, 27);
		stacksearch(stack, 6);
		System.out.println("stack empty? "+stack.empty());
		stack.removeElementAt(4);
		System.out.println("*after remove at index 4 "+ stack);
		System.out.println("Size: " + stack.size());
		
		Iterator<Integer> x = stack.iterator();
		while(x.hasNext()){
			System.out.println(x.next());
		}
		
		Stack s = new Stack();
		s.add(1);
		s.add("scaa");
		System.out.println(s);
	}
}

package collections.hw3;

import java.util.Collections;
import java.util.LinkedList;

public class StringCollection {

	public static void main(String[] args) {
		LinkedList<String> stringlist = new LinkedList<String>();
		stringlist.add("Abc");
		stringlist.add("Xyz");
		stringlist.add("Ile");
		stringlist.add("Sve");
		stringlist.add("Qop");
		stringlist.add("Gdt");
		stringlist.add("Dfu");

		System.out.println("Elements in list: \n" + stringlist);
		Collections.sort(stringlist, Collections.reverseOrder());
		System.out.println("Elements in list sorted in reverse order: \n" + stringlist);
	}

}

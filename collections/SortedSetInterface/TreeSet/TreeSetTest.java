package collections.SortedSetInterface.TreeSet;

import java.util.*;

public class TreeSetTest {

	public static void main(String[]args){
		SortedSet<String> set = new TreeSet<String>();
		
		//sorts elements in ascending order
		set.add("");
		set.add("2");
		set.add("Scaa");
		set.add("Scaa");
		set.add("Abc");
		set.add("Def");
		set.add("Zef");
		set.add("0ef");
		set.add("5ef");
		
		System.out.println(set);
		
		Iterator<String> itr = set.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
	}
}

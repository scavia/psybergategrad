package collections.SortedSetInterface.TreeSet2;

import java.util.*;

public class TreeSetTest {
	
	public static void main(String[]args){
		
		TreeSet<String> set = new TreeSet<String>();
		
		set.add("");
		set.add("abc");
		set.add("ufs");
		set.add("bdn");
		set.add("ndq");
		set.add("uwy");
		
		System.out.println(set);
		
		Iterator<String> itr = set.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
	}

}

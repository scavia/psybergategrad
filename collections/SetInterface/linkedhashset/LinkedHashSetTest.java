package collections.SetInterface.linkedhashset;

import java.util.*;

public class LinkedHashSetTest {
	
	public static void main(String[]args){
		
		LinkedHashSet<String> set = new LinkedHashSet<String>();
		set.add("Scaa");
		//no duplicates, contains unique elements
		set.add("Scaa");
		
		//at least one null value
		set.add("");
		set.add("");
		set.add("Abc");
		set.add("DEbc");
		
		//maintains insertion order
		System.out.println(set);
		
		Iterator<String> itr = set.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
	}

}

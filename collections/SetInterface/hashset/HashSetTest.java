package collections.SetInterface.hashset;

import java.util.*;

public class HashSetTest {
	
	public static void main(String[]args){
		
		HashSet<String> set = new HashSet<String>();
		set.add("Scaa");
		//no duplicates, contains unique elements
		set.add("Scaa");
		//at most one null value
		set.add("");
		set.add("");
		set.add("Abc");
		set.add("Cdef");
		
		//set.clear(); 
		System.out.println(set);
		
		//doesn't maintain insertion order
		Iterator<String> itr = set.iterator();
		String obj = "";
		while(itr.hasNext()){
			System.out.println(itr.next());
			obj = (String) itr.next();
			
			if(obj.equals("Abc")){
				itr.remove();
				System.out.println(obj + " removed!");
				break;
			}
		}
		
		System.out.println("\nNew Set...");
		System.out.println(set);
		
		//sorting 1
		List<String> list = new ArrayList<String>(set);
		Collections.sort(list);
		Collections.unmodifiableCollection(list);
		list.add("usa");
		System.out.println(list);
		
		//sorting 2
		TreeSet<String> x = new TreeSet<String>(set);
		System.out.println(x);
	}
}

package collections.hw2;

import java.util.HashMap;

public class EmployeeTest {
	
	
	public static void main(String[] args) {

		
		HashMap<Employee, Double> employee1 = new HashMap<Employee, Double>();
		employee1.put(new Employee(1, "Abc"), 550.5);
		employee1.put(new Employee(37, "Defg"), 3550.5);
		employee1.put(new Employee(20, "Gli"), 12550.5);
		employee1.put(new Employee(15, "Sxz"), 5750.5);
		employee1.put(new Employee(104, "Wqt"), 59650.5);
		employee1.put(new Employee(2000, "Mnu"), 44550.5);
		System.out.println(employee1);
	}

}

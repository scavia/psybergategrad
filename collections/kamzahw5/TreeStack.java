package collections.kamzahw5;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class TreeStack extends SortedStack {
	private static final int MIN_SIZE = 10;
	private Object[] arrStack = new Object[MIN_SIZE];
	private Comparator<?> comparator;
	private int size;

	public TreeStack() {
	}

	public TreeStack(Comparator<?> comparator) {
		super();
		this.comparator = comparator;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public boolean contains(Object o) {
		for (int i = 0; i < size; i++) {
			if (arrStack[i].equals(o)) return true;
		}
		return false;
	}

	@Override
	public Iterator iterator() {
		return new Iter();
	}

	@Override
	public Object[] toArray() {
		Object[] objects = new Object[size];
		for (int i = 0; i < size; i++) {
			objects[i] = get(i);
		}
		return objects;
	}

	@Override
	public boolean add(Object object) {
//		if (contains(object)) return false;
		push(object);
		return true;
	}

	@Override
	public boolean remove(Object o) {
		return false;
	}

	@Override
	public void clear() {
		arrStack = new Object[MIN_SIZE];
	}

	@Override
	public void push(Object object) {
		if (size == arrStack.length) {
			Object[] temp = arrStack;
			arrStack = new Object[size + MIN_SIZE];
			for (int i = 0; i < temp.length; i++) {
				arrStack[i] = temp[i];
			}
		}
		arrStack[size] = object;
		size++;

		// remember to fix this rubbish!!!!!!!!!!!!!!!!!!!!
		arrStack = toArray();
		List list = Arrays.asList(arrStack);
		if (comparator == null) {
			Collections.sort(list);// write by hand
		} else {
			Collections.sort(list, comparator);
		}
		arrStack = list.toArray();
	}

	private void sort(Object[] arrStack2) {
		for (int i = 0; i < size(); i++) {

		}
	}

	@Override
	public Object pop() {
		if (isEmpty()) throw new NoSuchElementException();
		Object objectToRemove = arrStack[size - 1];
		arrStack[size - 1] = null;
		return objectToRemove;
	}

	@Override
	public Object get(int position) {
//		if(position >= size) throw
		return arrStack[position];
	}

	@Override
	public String toString() {
		String myString = "[";
		boolean isFirst = true;
		for (int i = 0; i < size; i++) {
			if (isFirst) {
				myString += " " + arrStack[i];
				isFirst = false;
			} else myString += ", " + arrStack[i];
		}
		return myString + "]";
	}

	private class Iter implements Iterator<Object> {

		private int iteratorCount = size - 1;

		@Override
		public boolean hasNext() {
			if (iteratorCount >= 0) return true;
			return false;
		}

		@Override
		public Object next() {
			Object object = arrStack[iteratorCount];
			iteratorCount--;
			return object;
		}

	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection c) {
		for (Object object : c) {
			if (!contains(object)) push(object);
		}
		return true;
	}

	@Override
	public boolean removeAll(Collection c) {
		arrStack = new Object[MIN_SIZE];
		return true;
	}

	@Override
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

}


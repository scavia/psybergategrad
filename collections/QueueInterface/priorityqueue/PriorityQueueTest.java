package collections.QueueInterface.priorityqueue;

import java.util.*;

public class PriorityQueueTest {
	
	//maintains FIFO order, defined as an ordered list
	//holds elements or objects to be processed based on priority
	//doesnt allow null values
	
	public static void main(String[]args){
		
		PriorityQueue<String> queue = new PriorityQueue<String>();
		queue.add("abc");
		queue.add("def");
		queue.add("xbc");
		queue.add("");
		queue.add("");
		queue.contains("abc");
		queue.add("def");
		queue.add("ubc");
		queue.add("def");
		queue.add("defer"); 
		
		System.out.println(queue);
		
		System.out.println("head:"+queue.element());
		System.out.println("head:"+queue.peek());
		
		Iterator<String> itr = queue.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
				
	}

}

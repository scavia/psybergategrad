package factorialnumbersystem;


public class ProblemA023a {

	public static void main(String[] args) {

		printNumber(30);
	}

	private static void printNumber(int number) {
		String finalValue = " ";
		/*
		 * int n = number, higFac = 0; while (n != 0) { higFac =
		 * checkForFactorial(n); int facValue = calculateFactorial(higFac);
		 * System.out.println("div "+n/higFac); int noDeviser = divider(n,
		 * facValue); n -= noDeviser * facValue; finalValue += noDeviser; }
		 */

		int n = number;
		int highFac = checkForFactorial(n);
		while (n != 0) {
			int higFacRepetition = n / calculateFactorial(highFac);
			finalValue += higFacRepetition;
			n -= calculateFactorial(highFac) * higFacRepetition;
			highFac -= 1;
			if (n == 0 && highFac > 0) {
				for (int i = 0; i < highFac; i++)
					finalValue += 0;
			}

		}
		System.out.println(finalValue);
	}

	private static int checkForFactorial(int number)//calculates highest factorial
	{
		int n = 1;
		while (calculateFactorial(n) <= number) {
			n++;
		}
		return n - 1;
	}

	private static int divider(int toBeDevided, int devider) {
		int n = 0;
		while ((devider * n) <= toBeDevided) {
			n++;
		}
		return n - 1;
	}

	private static int calculateFactorial(int number) //calculate factorial
	{
		int a = 1;
		for (int i = 1; i <= number; i++) {
			a *= i;
		}
		return a;
	}

}

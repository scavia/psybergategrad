package AnnotationsImpl;

import AnnotationsImpl.annotations.DomainClass;
import AnnotationsImpl.annotations.DomainProperty;
import AnnotationsImpl.annotations.DomainTransient;

@DomainClass(tableName = "Cust")
public class Customer {

	@DomainProperty(colName = "CustomerNum")
	public String customerNum;
	
	@DomainProperty(colName = "Name")
	public String name;
	
	@DomainProperty(colName = "Surname")
	public String surname;
	
	@DomainProperty(colName = "Dob")
	public Integer dateOfBirth;
	
	@DomainTransient(colName = 0)
	public int age;
	
	public Customer(String customerNum, String name, String surname, Integer dateOfBirth, int age) {
		super();
		this.customerNum = customerNum;
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
		this.age = age;
	}

	public String getCustomerNum() {
		return customerNum;
	}

	public void setCustomerNum(String customerNum) {
		this.customerNum = customerNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Integer dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Customer [customerNum=" + customerNum + ", name=" + name + ", surname=" + surname + ", dateOfBirth="
				+ dateOfBirth + "]";
	}
	
//	public String toString(){
//		return "Customer Number: " + getCustomerNum() + " Name: " + getName()+ " Surname: " + getSurname()+ " DOB: " + getDateOfBirth();
//	}
	
	

}

package AnnotationsImpl;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

import AnnotationsImpl.annotations.DomainClass;
import AnnotationsImpl.annotations.DomainProperty;

public class CustomerService {

	private static Connection conn = null;
	private Class<?> sc = Customer.class;
	private String tableName = sc.getAnnotation(DomainClass.class).tableName();
	private Field[] cols = sc.getDeclaredFields();

	public static void dbConn() {

		String username = "postgres";
		String password = "scavia87";

		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/scavia1", username, password);
			System.out.println("Connected to the PostgreSQL server successfully.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void saveCustomer() {
		Customer customer = new Customer("Cust001", "Scavia", "Mlanga", 19790313, 7);
		
		if (Objects.isNull(customer)) {
			throw new RuntimeException("Object " + sc.getSimpleName() + " is null...");
		}
		
		String query = "INSERT INTO public." + tableName + "(";
		for (int i = 0; i < cols.length - 1; i++) {

			query += cols[i].getAnnotation(DomainProperty.class).colName();
			query += " ";

			if (i < cols.length - 2) {
				query += ", ";
			}
		}
		query += ")";
		query += "VALUES (?,?,?,?);";
		// System.out.println(query);

		try {
			dbConn();
			System.out.println("Inserting into table...");

			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, customer.getCustomerNum());
			ps.setString(2, customer.getName());
			ps.setString(3, customer.getSurname());
			ps.setInt(4, customer.getDateOfBirth());
			ps.executeUpdate();
			System.out.println("Inserting complete...");
			ps.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}

	public void getCustomer(String custNum) throws SQLException {

		dbConn();
		Statement stmt = conn.createStatement();

		String query = "SELECT * FROM public." + tableName + " WHERE CustomerNum = " + custNum;
		ResultSet rs = stmt.executeQuery(query);
		System.out.println("Retrieving from DB...");
		Customer user = null;
		if (rs.next()) {
			user = new Customer("", "", "", null, 0);
			// System.out.println(rs.getString(1));
			// System.out.println(rs.getString(2));
			// System.out.println(rs.getString("CustomerNum"));
			user.setCustomerNum(rs.getString(1));
			user.setName(rs.getString(2));
			user.setSurname(rs.getString(3));
			user.setDateOfBirth(rs.getInt(4));
			System.out.println(user);
			rs.close();
		}
		// return user;
	}
}

package AnnotationsImpl;

import java.sql.SQLException;

public class CustomerTest {

	public static void main(String[] args) {

		DatabaseManager db = new DatabaseManager();
		db.generateDatabase();
		CustomerService cs = new CustomerService();
		cs.saveCustomer();
		try {
			cs.getCustomer("'Cust001'");
			//System.out.println(cs.getCustomer("'Cust002'"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}

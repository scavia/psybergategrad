package AnnotationsImpl;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import AnnotationsImpl.annotations.DomainClass;
import AnnotationsImpl.annotations.DomainProperty;

public class DatabaseManager {

	private Connection conn = null;
	private Statement stmt = null;

	public void generateDatabase() {
		Class<?> c = Customer.class;

		if (!c.isAnnotationPresent(DomainClass.class)) {
			throw new RuntimeException(
					"The class " + c.getSimpleName() + " is not anotated with Domain Class annotation");
		}

		String tb = c.getAnnotation(DomainClass.class).tableName();
		Field[] cols = c.getDeclaredFields();

		String query = "CREATE TABLE IF NOT EXISTS PUBLIC." + tb + "(";
		for (int i = 0; i < cols.length - 1; i++) {
			// System.out.println(cols[i].getType().getSimpleName());
			// System.out.println("Field Collected...\n"+
			// cols[i].getAnnotation(DomainProperty.class).colName()+"\n");
			query += cols[i].getAnnotation(DomainProperty.class).colName();
			query += " ";
			query += columnType(cols[i].getType().getSimpleName());

			if (i < cols.length - 2) {
				query += ", ";
			}
		}
		query += ", PRIMARY KEY (customerNum)";
		query += ")";

//		System.out.println(query);

		try { 
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/scavia1", "postgres", "scavia87");
			System.out.println("Connected to the PostgreSQL server successfully.");
			System.out.println("Creating table...");
			stmt = conn.createStatement();

			stmt.executeUpdate(query);
			stmt.close();
			System.out.println("Table created successfully...");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}

	private String columnType(String type) {
		if (type.compareTo("String") == 0)
			return "VARCHAR (150) NOT NULL";
		else if (type.compareTo("Integer") == 0) {
			return "integer NOT NULL";
		}
		return "";
	}

}

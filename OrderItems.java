
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package orderitems;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;

/**
 *
 * @author scaa
 */

// Constructor using getter and setter metods and array initialiazation

public class OrderItems {
	private int customer_num;
	private String name;
	private String surname;
	private float price;
	private Date OrderDate;
	private Date DeliveryDate;
	private static int[] orders = new int[10];

	public OrderItems(int pcustomer_num, String pname, String psurname, float pprice, Date pOrderdate,
			Date pDeliverydate) {
		this.customer_num = pcustomer_num;
		this.name = pname;
		this.surname = psurname;
		this.price = pprice;
		this.OrderDate = pOrderdate;
		this.DeliveryDate = pDeliverydate;
	}

	public int orderTotal() {
		// System.out.println();
		return Arrays.stream(orders).sum();

	}

	public double orderTotalVAT() {
		return Arrays.stream(orders).sum() * 1.15;

	}

	public static void orderscaa(int order) {
		// index it to 0 ie z=0 if z=1 it displays 0 in array
		for (int z = 0; z < orders.length; z++) {
			if (orders[z] == 0) {
				orders[z] = order;
				break; // after adding one item stop adding
			}
		}
	}

	public int[] getAddItem() {
		return orders;
	}

	public int getCustomerNum() {
		return customer_num;
	}

	public String getName() {
		return name;
	}

	public String Surname() {
		return surname;
	}

	public float getPrice() {
		return price;
	}

	public Date getOrderDate() {
		return OrderDate;
	}

	public Date getDeliveryDate() {
		return DeliveryDate;
	}

	public void setCustomerNum(int pcustomer_num) {

		this.customer_num = pcustomer_num;
	}

	public void setName(String pname) {

		this.name = pname;
	}

	public void setSurName(String psurname) {

		this.surname = psurname;
	}

	public void setPrice(float pprice) {

		this.price = pprice;
	}

	public void setDate(Date pOrderdate) {

		this.OrderDate = pOrderdate;
	}

	public void setDeliveryDate(Date pDeliverydate) {

		this.DeliveryDate = pDeliverydate;
	}

	public static void main(String[] args) {

		OrderItems scavia = new OrderItems(20639975, "Scavia", "Mlanga", 234, new Date(30 - 02 - 2009),
				new Date(30 - 02 - 2009));
		System.out.println("CUSTOMER NUMBER   : " + scavia.customer_num);
		System.out.println("NAME              : " + scavia.getName());
		System.out.println("SURNAME           : " + scavia.Surname());
		// System.out.println("PRICE : $" + scavia.price);
		System.out.println("ORDER DATE        : " + LocalDate.now());
		System.out.println("DELIVERY DATE     : " + scavia.getDeliveryDate());
		OrderItems.orderscaa(1);
		scavia.orderscaa(3);
		scavia.orderscaa(23);
		orderscaa(2);
		//scavia.getAddItem();
		System.out.println(
				"TOTAL ORDER       : R" + scavia.orderTotal() + "\t" + "INC VAT    : R" + scavia.orderTotalVAT());

		System.out.println("ORDERS            : " + Arrays.toString(scavia.orders));
		// orderscaa(2);
		// System.out.println(Arrays.toString(orders));
	}

}

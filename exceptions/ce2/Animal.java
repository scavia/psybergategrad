package exceptions.ce2;

public class Animal {

	private String name;

	private int age;

	public Animal(String name, int age) throws MyAgeException {
		super();

		if (age < 0) {
			throw new MyAgeException("Invalid age enter age again... " + age);
		} else {
			this.name = name;
			this.age = age;
			System.out.println("New animal created of age "+age +" and name " +name);
		}
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public void eat() {
		if (getAge() < 5) {
			throw new MyEatingException("Age below 5 animal needs breastfeeding at age " + age);
		} else {
			System.out.println("Animal fit to graze");
		}
	}

}

package exceptions.ce2;

public class MyAgeException extends Exception {

	public MyAgeException(String message) {
		super(message);
	}

}

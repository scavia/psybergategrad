package exceptions.ce2;

public class AnimalTest {

	public static void main(String[] args) {
		Animal buffalo = null;

		try {
			buffalo = new Animal("Buffalo", 2);
			buffalo.eat();
		} catch (MyAgeException e) {
			System.out.println(e.getMessage());
		}
	}

}

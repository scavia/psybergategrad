package exceptions.ce2;

public class MyEatingException extends RuntimeException {

	public MyEatingException(String message) {
		super(message);
	}

}

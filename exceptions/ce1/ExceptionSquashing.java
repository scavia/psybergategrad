package exceptions.ce1;

public class ExceptionSquashing {

	private int balance = 500;

	public static void main(String[] args) {

		ExceptionSquashing ex = new ExceptionSquashing();

		try {
			ex.camera(5);
		} catch (Exception ignore) {

			// doesn't matter if ignored
		}

		ex.charges(600);

		try {
			ex.withdraw(800);
		} catch (Exception e) {
			// e.printStackTrace();
			// System.out.println("error");
		}

	}

	private void camera(int stop) {
		if (stop < 1) {
			System.out.println("cam started");
		} else {
			throw new IllegalStateException();
		}
	}

	public void withdraw(int witDraw) throws Exception {

		if (witDraw > 0 && witDraw < balance) {
			balance = balance - witDraw;
		} else {
			throw new Exception();
		}

	}

	public void charges(int charge) {

		if (charge > 0 && charge < balance) {
			balance -= charge;
		} else {
			// throw new IllegalStateException();
		}

	}

}

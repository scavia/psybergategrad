package exceptions.hw5;

public class Controller {

	private static AccountDB accountDB;
	private static AccountService service;

	public Controller() {
		accountDB = new AccountDB();
		// to matc accDB from service
		service = new AccountService(accountDB);
	}

	public void sendAccCreateCommand() throws AccountValidation {
		accountDB.createAccount();
	}

	public void sendAccWitdrawCommand() throws AccountValidation {
		service.witdraw();
	}

	public void sendAccViewCommand() throws AccountValidation {
		System.out.println(accountDB.printAccounts());
	}
}

package exceptions.hw5;

public class Account {

	private int AccountNum;

	private String Name;

	private int OpeningBalance;

	public Account(int accountNum, String name, int openingBalance) {
		this.AccountNum = accountNum;
		this.Name = name;
		this.OpeningBalance = openingBalance;
	}

	public int getAccountNum() {
		return AccountNum;
	}

	public void setAccountNum(int accountNum) {
		AccountNum = accountNum;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getOpeningBalance() {
		return OpeningBalance;
	}

	public void setOpeningBalance(int openingBalance) {
		OpeningBalance = openingBalance;
	}

	public void withdraw(int amount) throws AccountValidation {
		if (amount > OpeningBalance) {
			throw new AccountValidation();
		} else {
			OpeningBalance -= amount;
		}
	}

	//for reference by contains method
	public boolean equals(Object object) {
		if (object == null)
			return false;
		if (this == object)
			return true;
		if (getClass() != object.getClass())
			return false;
		Account otherAccnum = (Account) object;
		if (this.AccountNum == otherAccnum.AccountNum)
			return true;
		return false;
	}

}

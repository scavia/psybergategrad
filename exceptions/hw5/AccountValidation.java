package exceptions.hw5;

public class AccountValidation extends Exception {

	private static final long serialVersionUID = 1L;

	public AccountValidation() {
		super();
	}

	public AccountValidation(String message, Throwable cause) {
		super(message, cause);
	}

	public AccountValidation(String message) {
		super(message);
	}

}

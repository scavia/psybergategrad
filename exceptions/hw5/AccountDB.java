package exceptions.hw5;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AccountDB {

	private List<Account> accounts;

	private Scanner inpDetails;

	public AccountDB() {

		accounts = new ArrayList<Account>();
		inpDetails = new Scanner(System.in);
	}

	public void createAccount() throws AccountValidation {

		int accNum;
		String name;
		int balance;

		System.out.println("Enter details: \nAccount number: ");
		accNum = inpDetails.nextInt();
		System.out.println("Customer name: ");
		name = inpDetails.next();
		System.out.println("Opening balance: ");
		balance = inpDetails.nextInt();

		if (accNum % 11 == 0 && accNum > 100 && balance > 1000) {
			insertAccount(accNum, name, balance);
		} else {
			throw new AccountValidation("Account number must be > 100, divisible by 11 & balance > 1000");
		}

	}

	public void insertAccount(int accountNum, String name, int openingBalance) throws AccountValidation {
		Account account = new Account(accountNum, name, openingBalance);
		if (accounts.contains(account)) {
			throw new AccountValidation("Account already exists...");
		}
		accounts.add(account);
	}

	public void witdrawMoney(int accNumb, int amount) throws AccountValidation {

		boolean checkAcc = false;
		for (Account account : accounts) {
			if (account.getAccountNum() == accNumb) {
				checkAcc = true;
				account.withdraw(amount);
				System.out.println("\nWitdraw on Account Number: " + account.getAccountNum() +" accepted..." + "Amount: " + amount +
									"\nNew Balance: " + account.getOpeningBalance());
			}
		}
		if (!checkAcc) {
			throw new AccountValidation("Account doesn't exist...");
		}
	}

	public String printAccounts() {
		//cec size of arraylist accounts.size()
		System.out.println("\nNumber of accounts on system...: " + accounts.size());
		String details = "";
		for (Account account : accounts) {
			details += "______________________________";
			details += "\nName: \t" + account.getName() + "\nAccount Number: " + account.getAccountNum()
					+ "\nBalance: \t" + account.getOpeningBalance();
			details += "\n_____________________________";
		}
		return details.toUpperCase();

	}

}

package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexText {

	public static void main(String[] args) {
		String input = "Hello gooday classes End a123478";
		String regex = "e";
		Pattern pattern = Pattern.compile(regex);
		Pattern pattern1 = Pattern.compile("e.");//pics e and any next caracter
		Pattern pattern2 = Pattern.compile("e|E");//pics a or E
		Pattern pattern3 = Pattern.compile("[aoncrwei][lmbvxzaee]");//pics any 2 caracters from set a & b in our string
		Pattern pattern4 = Pattern.compile("[1-9][0-9]{7}");// numbers>=1 000 000 
		Pattern pattern5 = Pattern.compile("[a-i]{1}[0-9]{4}");// numbers>=1 000 000 
		Matcher matcher = pattern.matcher(input);
		Matcher matcher1 = pattern1.matcher(input);
		Matcher matcher2 = pattern2.matcher(input);
		Matcher matcher3 = pattern3.matcher(input);
		Matcher matcher4 = pattern5.matcher(input);
		//greedy possessive reluctant
		while (matcher1.find()) {
			System.out.println("");
			System.out.println("..."+matcher1.group());
		}
		
		//bac referencing
		//alteration
		//caracterlass
		//caracter
		//grouping
		//javaregex
		//quantifiers
		//regexutils

		while (matcher2.find()) {
			System.out.println("");
			System.out.println(matcher2.group());
		}
		
		while (matcher4.find()) {
			System.out.println("");
			System.out.println(matcher4.group());
		}

		while (matcher3.find()) {
			System.out.println("");
			System.out.println(matcher3.group());
		}

		int from = 0;
		int count = 0;
		while (matcher.find(from)) {
			count++;
			from = matcher.start() + 1;
		}
		System.out.println(count);
	}

}

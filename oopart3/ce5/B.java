package oopart3.ce5;

//B extends (inherits) A. When a class extends a abstract class,
//it is said to “implement” the abstract class.

public class B extends A {

	public void triple3() {
		// must be defined, else compiler makes a complaint.
		// Also, all return type and parameter must agree with the parent class.

		x = x * 3 + 1;
	}
	
/*	private abstract class D{
		
	}*/


}

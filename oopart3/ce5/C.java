package oopart3.ce5;

public class C {

	public static void main(String[] args) {

		// A obj = new A(); // abstract class cannot be instantiated
		A obj0 = new B(); // abstract class cannot be instantiated but used to
							// create object references
		obj0.triple(3);
		System.out.println(obj0.returnMe());
		B obj1 = new B();
		obj1.triple3();
		System.out.println(obj1.returnMe());
	}

	// java.lang.Number is a abstract class.
	// What is the difference between abstract class and interface?

}

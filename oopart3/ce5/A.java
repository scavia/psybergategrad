package oopart3.ce5;

/*
 * 
 * abstract is a non-access modifier on class & methods declaration but not variables
 * abstract classes are meant to serve as a parent of classes, for the purpose of extension 
 * (inheritance) by other classes
 * Any class that contains one or more abstract methods must also be declared abstract
 * 
*/

public abstract class A {

	public int x;

	public int y; // variables cannot be abstract

	public A() {
		x = 1; // constructor cannot be abstract
	}

	public void triple(int n) {
		x = x * 3; // a normal method
	}

	public static int triple2(int n) {
		return n * 3; // a static method in abstract class is ok.
	}

	public abstract void triple3(); // abstract method. has no definition.

	// static abstract void triple3 (int n); // abstract static cannot be
	// combined
	// abstract static
	// abstract private
	// final - used to prevent ineritence yet parent class depends on cild for
	// complete
	// implementation
	// incase of metods final stops overiding yet abstract metods need to be
	// overiden

	public int returnMe() {
		return x;
	}

}

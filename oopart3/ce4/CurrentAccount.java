package oopart3.ce4;

public class CurrentAccount extends Account {

	 protected String acctype;

	public CurrentAccount(String accountNum, String name, String sname, double balance) {
		super(accountNum, name, sname, balance);
		
	}
	
	public CurrentAccount() {
		this(" ", " ", " ",0);	
	}
	
	public Account withdraw(int witDraw) {
		
		double witDrawCharge = 3.555;
		
		Account obj = new Account(this.accountNum, this.name, this.sname, this.balance);
		if (witDraw > 0 && witDraw < balance && accountNum.equals(this.accountNum)) {
			this.balance = balance - (witDraw + witDrawCharge);
			obj.balance = this.balance;
		} else {
			throw new IllegalStateException("error");
		}
		return obj;
	}

}

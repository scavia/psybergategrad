package oopart3.ce4;

public class Account {

	protected String accountNum;

	protected String name;

	protected String sname;

	protected double balance;

	public Account(String accountNum, String name, String sname, double balance) {
		super();
		this.accountNum = accountNum;
		this.name = name;
		this.sname = sname;
		this.balance = balance;
	}

	protected String getAccountNum() {
		return accountNum;
	}

	protected String getName() {
		return name;
	}

	protected String getSname() {
		return sname;
	}

	protected double getBalance() {
		return balance;
	}

	public Account withdraw(int witDraw) {

		// setBalance(getBalance()- witDraw); setter must be there // also
		// witraw

		Account obj = new Account(this.accountNum, this.name, this.sname, this.balance);
		if (witDraw > 0 && witDraw < balance) {
			this.balance = balance - witDraw;
			obj.balance = this.balance;
		} else {
			throw new IllegalStateException("error");
		}
		return obj;
		
		//return this;
	}

	public Account Deposit(int amount) {
		
		//setBalance(getBalance()- amount); 
		return this;

	}

	public void withdraw(double witdrawAmount) {

		if (witdrawAmount > 0 && witdrawAmount > balance) {
			System.out.println("exeeds acc bal");
			// throw new IllegalStateException("error");
			return;
		} else
			balance -= witdrawAmount;
	}
}

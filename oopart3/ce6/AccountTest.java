package oopart3.ce6;

import java.util.ArrayList;

public class AccountTest {

	public static void main(String[] args) {

		// lists

		ArrayList<CurrentAccount> CurrentAccounts = new ArrayList<CurrentAccount>();
		CurrentAccounts.add(new CurrentAccount("YT6786X-Stanbic", "scaa", "mhl", 6000));
		CurrentAccounts.add(new CurrentAccount("YT6786X-Capitec", ".....x", "::::x", 6000));
		CurrentAccounts.add(new CurrentAccount("YT6786X-Stanbic", "scaa", "mhl", 6000));

		CurrentAccount obj1 = new CurrentAccount("YT6786X-First Nat", "scaa", "mhl", 6000);
		CurrentAccounts.add(obj1);
		CurrentAccount obj2 = obj1;

		printAccCollection(CurrentAccounts);

		System.out.println("");

		// check

		boolean check = CurrentAccounts.contains(obj1);

		if (check == true) {
			System.out.println("Object IS contained in the list");
		} else {
			System.out.println("Object ISNT contained in the list");
		}
		
		//equals
		
		System.out.println(obj2.equals(obj1));

	}

	public static void printAccCollection(ArrayList<CurrentAccount> display) {
		for (CurrentAccount account : display) {
			System.out.println("Account Number: " + account.getAccountNum() + " Balance : " + account.getBalance());
		}
	}
}

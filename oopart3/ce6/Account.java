package oopart3.ce6;

public class Account {

	protected String accountNum;

	protected String name;

	protected String sname;

	protected double balance;

	public Account(String accountNum, String name, String sname, double balance) {
		super();
		this.accountNum = accountNum;
		this.name = name;
		this.sname = sname;
		this.balance = balance;
	}

	protected String getAccountNum() {
		return accountNum;
	}

	protected String getName() {
		return name;
	}

	protected String getSname() {
		return sname;
	}

	protected double getBalance() {
		return balance;
	}

}

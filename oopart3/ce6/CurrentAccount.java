package oopart3.ce6;

public class CurrentAccount extends Account {

	protected String acctype;

	public CurrentAccount(String accountNum, String name, String sname, double balance) {
		super(accountNum, name, sname, balance);

	}

	public CurrentAccount() {
		this(" ", " ", " ", 0);
	}

}

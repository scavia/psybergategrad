package oopart3.ce3;

public class CurrentAccount extends Account {

	protected float balance;

	{
		System.out.println("Current Account instance");
	}

	public CurrentAccount(String accountNum, String name, String sname, float balance) {
		super(accountNum, name, sname);

		this.balance = balance;

		System.out.println("Class Current Account:" + balance);

		System.out.println("CurrentAccount::=>" + this.toString());
	}

}

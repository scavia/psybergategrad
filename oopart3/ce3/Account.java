package oopart3.ce3;

public class Account {

	protected String accountNum;
	protected String name;
	protected String sname;
	
	{
		System.out.println("Account instance");
	}

	public Account(String accountNum, String name, String sname) {
		super();
		this.accountNum = accountNum;
		this.name = name;
		this.sname = sname;

		System.out.println("Class Account: " + accountNum + "\nClass Account: " +name +"\nClass Account: " + sname);
		System.out.println("Account::=>"+this.toString());
	}

}

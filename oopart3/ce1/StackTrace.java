package oopart3.ce1;

public class StackTrace {

	public static void main(String[] args) throws Exception {

		try {

			testException1();
		}

		catch (Throwable e) {

			e.printStackTrace();
			
			// throws error out of main and says error in main
			throw e;

		}
	}

	// method which throws Exception
	public static void testException1() throws Exception {
		testException2();
	}

	public static void testException2() throws Exception {
		testException3();
	}
	
	public static void testException3() throws Exception {
		testException4();
	}

	public static void testException4() throws Exception {
		testException5();
	}

	public static void testException5() throws Exception {
		throw new Exception("Stack Tracek!");
	}


}

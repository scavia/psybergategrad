package oopart3.ce2;

public class AnimalTest {

	public static void main(String[] args) {
		
		Dog dog = new Dog("Bubbly", 5);
		Animal animal = dog;
		dog.display();
		System.out.println(dog.toString());
		System.out.println(((Animal)animal).toString());
	}

}

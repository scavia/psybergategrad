package oopart3.ce2;

/*
 * this doesn't work well in a method because it calls that current method recursively and stack overflow appens
*/

public class Animal {

	protected String name;

	protected int age;

	public Animal(String name) {

		this.name = name;

	}

	public Animal(String name, double year) {

		//this(doSomething(5)); wont compile because super hasn't executed data hasn't been initiated unless doSometig is static
		this(name);
		
	}
	
	public Animal() {

		//this(doSomething(5)); wont compile because super hasn't executed data hasn't been initiated unless doSometig is static
		
		//this(); bot words wont work @ the same tym
		super();
		
	}

	public void display() {
		// this.display();
		System.out.println("Name :" + name);
	}

	public void doSomething(int i) {

	}
	
	public String toString() {
		return Animal.class.toString();
	}

}

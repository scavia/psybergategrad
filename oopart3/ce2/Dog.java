package oopart3.ce2;

public class Dog extends Animal {

	public Dog(String name, int age) {
		super(name);

		this.age = age;

	}

	public void display() {
		super.display();
		System.out.println("Name :" + name + "\nAge  :" + age + " years old");
	}
	
	public void doSomething(int i){
		
	}
	
	public String toString() {
		return Dog.class.toString();
	}

}

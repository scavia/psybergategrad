package oopolymorph.hw3;

import java.util.ArrayList;

public class PrintAccounts {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Account acc0 = new CurrentAccount(1,3000);
		Account acc1 = new SavingsAccount(2,3000);
		printAcc(acc0);
		printAcc(acc1);
		
		//populating list
		ArrayList<Account> arraypopulate = new ArrayList<Account>();
		arraypopulate.add(new CurrentAccount(3,2000));
		arraypopulate.add(new SavingsAccount(4,3500));
		//call display metod
		printAccCollection(arraypopulate);
		
	}
	
	public static void printAcc(Account acc){
		System.out.println("Account Number: "+ acc.getAccountNum() + " Balance : "+ acc.getBalance());
	}
	
	//collection mettod
	public static void printAccCollection(ArrayList<Account> array1){
		for(Account account : array1){
			System.out.println("Account Number: "+ account.getAccountNum() + " Balance : "+ account.getBalance());	
		}
		
	}

}

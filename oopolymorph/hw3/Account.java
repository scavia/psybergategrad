package oopolymorph.hw3;

public class Account {

	int accountNum;
	int balance;

	public Account(int aN, int b) {

		this.accountNum = aN;
		this.balance = b;
	}

	public int getAccountNum() {
		return accountNum;
	}

	public int getBalance() {
		return balance;
	}

}

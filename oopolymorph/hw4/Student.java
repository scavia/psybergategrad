package oopolymorph.hw4;

import java.util.LinkedList;
import java.util.List;

public class Student {

	public String name;

	public String surname;

	public String reg_num;

	public String student_type;
	
	public String library_access;
	
	//public String ;

	private List<Student> students;

	public Student(String name, String surname, String reg_num, String student_type) {
		super();
		this.name = name;
		this.surname = surname;
		this.reg_num = reg_num;
		this.student_type = student_type;
		students = new LinkedList<Student>();
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getReg_num() {
		return reg_num;
	}

	public String getStudent_type() {
		return student_type;
	}

	public List<Student> getStudents() {
		return students;
	}
	
	

}

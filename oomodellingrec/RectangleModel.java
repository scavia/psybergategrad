package oomodellingrec;

public class RectangleModel {

	// static variable
	public static final int MAXIMUM_LENGTH = 100;

	private int length;

	private int width;

	public RectangleModel(int l, int w) {
		// validation
		if (l < MAXIMUM_LENGTH && w >= 0 && l >= 0 && w < l) {
			this.length = l;
			this.width = w;
		} else {
			System.out.println("Invalid length or width!");
		}
	}

	public int getLength() {
		return length;
	}

	public int getWidth() {
		return width;
	}

	public int getPerimeter() {
		return ((length * 2) + (width * 2));
	}

	public int getArea() {
		return (length * width);
	}

	// static method
	public static void displayRec(RectangleModel... r) {
		for (RectangleModel rec : r) {
			System.out.println("Length: " + rec.getLength() + " Width: " + rec.getWidth());
			System.out.println("Perimeter: " + rec.getPerimeter());
			System.out.println("Area: " + rec.getArea());
			System.out.println(" ");
		}
	}

	public boolean equals(RectangleModel R) {
		if (R == null)
			return false;
		// wen casting using obect class
		// RectangleModel rec = (RectangleModel) obj;
		return this.length == R.length && this.width == R.width;

	}

	public static void main(String[] args) {
		// Create 5 Rectangle objects that represent 5 different Rectangles:
		// for each: object reference is different
		// for each: object type is similar RectangleModel
		// for each object there is unique state length and width
		// in memory they have different memory locations<identity>
		RectangleModel rec1 = new RectangleModel(21, 10);
		displayRec(rec1);
		RectangleModel rec2 = new RectangleModel(16, -14);
		displayRec(rec2);
		RectangleModel rec3 = new RectangleModel(8, 10);
		displayRec(rec3);
		RectangleModel rec4 = new RectangleModel(11, 8);
		displayRec(rec4);
		RectangleModel rec5 = new RectangleModel(2, 0);
		displayRec(rec5);
		// Write your own code using the Rectangle object to demonstrate Java
		// identity(==)
		RectangleModel rec6 = rec5;
		System.out.println(rec6 == rec5);
		// Write your own code using the Rectangle object to demonstrate Java
		// equivalence (equality)(=)
		System.out.println(rec6.equals(rec5));
		// array usage(for loop ten ...standing any number of items to place in
		// array)
		System.out.println(" ");
		displayRec(rec5, rec1);
	}
}

package concurrency.ce4;

public class Singleton {

	private static Singleton x = new Singleton();

	public Singleton() {
	}

	public static Singleton getInstance() {
		return x;
	}

}

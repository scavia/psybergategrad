package concurrency.ce3_3;

public class Calculator implements Runnable {

	private static int counter = 0;
	private static int sum = 0;

	private static int getAndIncrement() {
		int temp = counter;
		counter = counter + 1;
		return temp;
	}

	private void addToSum(int value) {
		sum += value;
	}

	@Override
	public void run() {
		while (counter <= 100000000L) {
			int tempVal = getAndIncrement();
			addToSum(tempVal);
		}

	}

	public static void main(String[] args) throws InterruptedException {

		Calculator[] calc = new Calculator[8];
		Thread[] thread = new Thread[8];

		final long startTime = System.currentTimeMillis();

		for (int i = 0; i < 8; i++) {
			calc[i] = new Calculator();
			thread[i] = new Thread(calc[i]);
			Thread.sleep(1000);
			thread[i].start();
			thread[i].join();
			System.out.println(thread[i].currentThread().getName());
		}

		// for (int z = 0; z < 8; z++) {
		// thread[z].join();
		// }

		while (thread[0].isAlive() || thread[1].isAlive() || thread[2].isAlive() || thread[3].isAlive()
				|| thread[4].isAlive() || thread[5].isAlive() || thread[6].isAlive() || thread[7].isAlive()) {
		}

		final long endTime = System.currentTimeMillis();
		System.out.println(Calculator.sum);
		System.out.println("Execution time : " + (endTime - startTime) / 1000d + "secs");
	}

}

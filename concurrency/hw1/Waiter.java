package concurrency.hw1;

public class Waiter implements Runnable {

	private Message msg;

	public Waiter(Message m) {
		super();
		this.msg = m;
	}

	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		synchronized (msg) {
			try {
				System.out.println(name + " waiting to get notified at: " + System.currentTimeMillis()/1000d+"secs");
				msg.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(name + " got notified at: " + System.currentTimeMillis()/1000d+"secs");
			System.out.println(name + " processed: " + msg.getMsg());
		}
	}

}

package concurrency.ce1;

public class ConcurrentThread implements Runnable{

	   Thread mythread ;
	   ConcurrentThread()
	   { 
	      mythread = new Thread(this, "my runnable thread");
	      System.out.println("my thread created" + mythread);
	      mythread.start();
	   }
	   
	   public static void main(String[] args)     {
		   ConcurrentThread cnt = new ConcurrentThread();
	       try
	       {
	          while(cnt.mythread.isAlive())
	          {
	            System.out.println("Main thread will be alive till the child thread is live"); 
	            Thread.sleep(1500);
	          }
	       }
	       catch(InterruptedException e)
	       {
	          System.out.println("Main thread interrupted");
	       }
	       System.out.println("Main thread run is over" );
	    }
	   public void run()
	   {
	      try
	      {
	        for (int i=0 ;i<3;i++)
	        {
	          System.out.println("Printing the count " + i);
	          Thread.sleep(1000);
	        }
	     }
	     catch(InterruptedException e)
	     {
	        System.out.println("my thread interrupted");
	     }
	     System.out.println("mythread run is over" );
	   }
	}



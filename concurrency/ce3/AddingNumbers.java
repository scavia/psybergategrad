package concurrency.ce3;

public class AddingNumbers implements Runnable {

	private int a;

	private int b;

	public AddingNumbers(int a, int b) {
		super();
		this.a = a;
		this.b = b;
	}

	@Override
	public void run() {
		try {
			add(a, b);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void add(int a2, int b2) throws InterruptedException {
		int sum = 0;
		for (int i = a; i <= b; i++) {
			sum += i;
			//Thread.sleep(100);
		}

		System.out.println("Sum of numbers from " + a + " to " + b + " is = " + sum);
	}

}

package ooclientmodel;

import java.util.LinkedList;

public class ClientModel {

	private static String customernum;

	private static String name;

	private static String surname;

	private static String phonenumber;

	private static String idnumber;

	public ClientModel(String ccn, String cnm, String csm, String cpn) {
		customernum = ccn;
		name = cnm;
		surname = csm;
		phonenumber = cpn;

	}

	public ClientModel(String cn, String nm, String pn) {
		idnumber = cn;
		name = nm;
		phonenumber = pn;

	}

	public boolean equals(ClientModel CM) {
		if (CM == null)
			return false;
		return this.customernum == CM.customernum;
	}

	public static void main(String[] args) {
		new ClientModel("1004", "xaa", "mlanga", "2776");
		LinkedList<String> NaturalClient = new LinkedList<String>();
		NaturalClient.add(0, customernum);
		NaturalClient.add(1, name);
		NaturalClient.add(2, surname);
		NaturalClient.add(3, phonenumber);
		displayDetails(NaturalClient);

		new ClientModel("10504", "varie", "zzziizzz", "27786576");
		LinkedList<String> NnaturalClient = new LinkedList<String>();
		NnaturalClient.add(0, customernum);
		NnaturalClient.add(1, name);
		NnaturalClient.add(2, surname);
		NnaturalClient.add(3, phonenumber);
		displayDetails(NnaturalClient);

		new ClientModel("155", "Psybergate", "00287976");
		LinkedList<String> CompanyClient = new LinkedList<String>();
		CompanyClient.add(0, idnumber);
		CompanyClient.add(1, name);
		CompanyClient.add(2, phonenumber);
		displayDetails(CompanyClient);

		new ClientModel("1575", "Stanbic", "0076");
		LinkedList<String> CcompanyClient = new LinkedList<String>();
		CcompanyClient.add(0, idnumber);
		CcompanyClient.add(1, name);
		CcompanyClient.add(2, phonenumber);
		displayDetails(CcompanyClient);

		System.out.println("");
		System.out.println(NaturalClient == CompanyClient);
		System.out.println(NaturalClient.equals(NaturalClient));
		System.out.println(NaturalClient.equals(CompanyClient));

	}

	private static void displayDetails(LinkedList<String> naturalClient) {

		if (idnumber == null) {
			System.out.println("NATCL DETAILS : " + naturalClient);
		} else /* if (idnumber != null) */ {
			System.out.println("COMPCL DETAILS: " + naturalClient);
		}

	}
}

package ooencapsulation;

public class SavingsAccount extends Account {

	public static final double MIN_BALANCE = 5000;

	public SavingsAccount(long account_num, double balance) {
		super(account_num, balance);

		balance = MIN_BALANCE;
	}

	public boolean overdrwan() {
		if (getBalance() < MIN_BALANCE && getBalance() < 0) {
			return true;
		}
		return false;
	}

	public boolean toReview() {
		if (getBalance() < 0) {
			return true;
		}
		return false;

	}

	// implementation of abtract metod
	public boolean oveerdrawn() {
		return false;
	}

	public double getBalance() {
		return (super.getBalance() + 1000);
	}
}

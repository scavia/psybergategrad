package ooencapsulation;

import java.util.List;

public interface ClientAccounts {

	public long getClientnum();

	public String getName();

	public String getAddress();

	public List<Account> getAccounts();

	public void addAccount(Account account);

	public double getAmountBalance(Client clie);

	public double totalBalance();

}

package ooencapsulation;

import java.util.LinkedList;
import java.util.List;

public class Client implements ClientAccounts{

	public long clientnum;

	public String name;

	public String address;

	private List<Account> userAccounts;

	public Client(long clientnum, String name, String address) {
		super();
		this.clientnum = clientnum;
		this.name = name;
		this.address = address;
		userAccounts = new LinkedList<Account>();
	}

	public long getClientnum() {
		return clientnum;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public List<Account> getAccounts() {
		return userAccounts;

	}

	public void addAccount(Account account) {
		userAccounts.add(account);
	}

	public double getAmountBalance(Client clie) {
		return clie.totalBalance();

	}
	
	public double totalBalance() {
		double amount = 0.0;
		for (Account account : userAccounts) {
			if (account instanceof CurrentAccount) {
				amount += account.getBalance();
			}
		}
		return amount;
	}

}

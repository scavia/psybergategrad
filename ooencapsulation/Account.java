package ooencapsulation;

public abstract class Account {

	private final long account_num;
	double balance;

	public Account(long account_num, double balance) {
		super();

		if (account_num >= 0L && balance >= 0.0) {
			this.account_num = account_num;
			this.balance = balance;
		} else {
			throw new RuntimeException("Account Error!");
		}
	}

	public long getAccount_num() {
		return account_num;
	}

	/*
	 * returning specific account number or if you want to return specific
	 * account type eg "Current Acc" public long getAccount_num() { return
	 * 657263558L; }
	 */

	public double getBalance() {
		return balance;
	}

	public boolean overdrawn() {
		return balance < 0;
	}

	public abstract boolean toReview();

	public abstract boolean oveerdrawn();

	public double needsReview() {
		if (balance > 0)
			return 0;
		else {
			return -(balance);
		}
		//return (overdrawn() && balance < (-(MAXOVERDRAFT*(1+0.2)))) || balance <= -5000;
	}

}

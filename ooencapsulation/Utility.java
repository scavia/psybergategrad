package ooencapsulation;

public class Utility {

	/*
	 * public double getAmountBalance(Client clie) { return clie.totalBalance();
	 * 
	 * }
	 */

	public static void main(String[] args) {
		Client c = new Client(120, "Scavia", "Unit 2, Texas Complex, Wasington USA");
		
		c.addAccount(new CurrentAccount(1287886313099L, 3110.99));
		c.addAccount(new CurrentAccount(1287886313099L, 3110.99));
		c.addAccount(new CurrentAccount(1287886313099L, 3110.99));
		c.addAccount(new CurrentAccount(1287886313099L, 0));
		c.addAccount(new SavingsAccount(1287886313099L, 3110.99));
		printAcc(c);
		// System.out.println(new Utility().getAmountBalance(c));
	}

	public static void printAcc(Client acc) {
		System.out.println("Client Number	: " + acc.getClientnum() + "\nName		: " + acc.getName() + "\nAddress		: "
				+ acc.getAddress() + "\nAccount Balance	: " + acc.getAmountBalance(acc));
	}

}
